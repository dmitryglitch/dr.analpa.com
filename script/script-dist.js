

!function(e) {
    "use strict";
    "function" == typeof define && define.amd ? define([ "jquery" ], e) : "undefined" != typeof exports ? module.exports = e(require("jquery")) : e(jQuery);
}(function(d) {
    "use strict";
    var s, r = window.Slick || {};
    s = 0, (r = function(e, t) {
        var i, o = this;
        o.defaults = {
            accessibility: !0,
            adaptiveHeight: !1,
            appendArrows: d(e),
            appendDots: d(e),
            arrows: !0,
            asNavFor: null,
            prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">Previous</button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button">Next</button>',
            autoplay: !1,
            autoplaySpeed: 3e3,
            centerMode: !1,
            centerPadding: "50px",
            cssEase: "ease",
            customPaging: function(e, t) {
                return d('<button type="button" data-role="none" role="button" tabindex="0" />').text(t + 1);
            },
            dots: !1,
            dotsClass: "slick-dots",
            draggable: !0,
            easing: "linear",
            edgeFriction: .35,
            fade: !1,
            focusOnSelect: !1,
            infinite: !0,
            initialSlide: 0,
            lazyLoad: "ondemand",
            mobileFirst: !1,
            pauseOnHover: !0,
            pauseOnFocus: !0,
            pauseOnDotsHover: !1,
            respondTo: "window",
            responsive: null,
            rows: 1,
            rtl: !1,
            slide: "",
            slidesPerRow: 1,
            slidesToShow: 1,
            slidesToScroll: 1,
            speed: 500,
            swipe: !0,
            swipeToSlide: !1,
            touchMove: !0,
            touchThreshold: 5,
            useCSS: !0,
            useTransform: !0,
            variableWidth: !1,
            vertical: !1,
            verticalSwiping: !1,
            waitForAnimate: !0,
            zIndex: 1e3
        }, o.initials = {
            animating: !1,
            dragging: !1,
            autoPlayTimer: null,
            currentDirection: 0,
            currentLeft: null,
            currentSlide: 0,
            direction: 1,
            $dots: null,
            listWidth: null,
            listHeight: null,
            loadIndex: 0,
            $nextArrow: null,
            $prevArrow: null,
            slideCount: null,
            slideWidth: null,
            $slideTrack: null,
            $slides: null,
            sliding: !1,
            slideOffset: 0,
            swipeLeft: null,
            $list: null,
            touchObject: {},
            transformsEnabled: !1,
            unslicked: !1
        }, d.extend(o, o.initials), o.activeBreakpoint = null, o.animType = null, o.animProp = null, 
        o.breakpoints = [], o.breakpointSettings = [], o.cssTransitions = !1, o.focussed = !1, 
        o.interrupted = !1, o.hidden = "hidden", o.paused = !0, o.positionProp = null, o.respondTo = null, 
        o.rowCount = 1, o.shouldClick = !0, o.$slider = d(e), o.$slidesCache = null, o.transformType = null, 
        o.transitionType = null, o.visibilityChange = "visibilitychange", o.windowWidth = 0, 
        o.windowTimer = null, i = d(e).data("slick") || {}, o.options = d.extend({}, o.defaults, t, i), 
        o.currentSlide = o.options.initialSlide, o.originalSettings = o.options, void 0 !== document.mozHidden ? (o.hidden = "mozHidden", 
        o.visibilityChange = "mozvisibilitychange") : void 0 !== document.webkitHidden && (o.hidden = "webkitHidden", 
        o.visibilityChange = "webkitvisibilitychange"), o.autoPlay = d.proxy(o.autoPlay, o), 
        o.autoPlayClear = d.proxy(o.autoPlayClear, o), o.autoPlayIterator = d.proxy(o.autoPlayIterator, o), 
        o.changeSlide = d.proxy(o.changeSlide, o), o.clickHandler = d.proxy(o.clickHandler, o), 
        o.selectHandler = d.proxy(o.selectHandler, o), o.setPosition = d.proxy(o.setPosition, o), 
        o.swipeHandler = d.proxy(o.swipeHandler, o), o.dragHandler = d.proxy(o.dragHandler, o), 
        o.keyHandler = d.proxy(o.keyHandler, o), o.instanceUid = s++, o.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/, 
        o.registerBreakpoints(), o.init(!0);
    }).prototype.activateADA = function() {
        this.$slideTrack.find(".slick-active").attr({
            "aria-hidden": "false"
        }).find("a, input, button, select").attr({
            tabindex: "0"
        });
    }, r.prototype.addSlide = r.prototype.slickAdd = function(e, t, i) {
        var o = this;
        if ("boolean" == typeof t) i = t, t = null; else if (t < 0 || t >= o.slideCount) return !1;
        o.unload(), "number" == typeof t ? 0 === t && 0 === o.$slides.length ? d(e).appendTo(o.$slideTrack) : i ? d(e).insertBefore(o.$slides.eq(t)) : d(e).insertAfter(o.$slides.eq(t)) : !0 === i ? d(e).prependTo(o.$slideTrack) : d(e).appendTo(o.$slideTrack), 
        o.$slides = o.$slideTrack.children(this.options.slide), o.$slideTrack.children(this.options.slide).detach(), 
        o.$slideTrack.append(o.$slides), o.$slides.each(function(e, t) {
            d(t).attr("data-slick-index", e);
        }), o.$slidesCache = o.$slides, o.reinit();
    }, r.prototype.animateHeight = function() {
        var e = this;
        if (1 === e.options.slidesToShow && !0 === e.options.adaptiveHeight && !1 === e.options.vertical) {
            var t = e.$slides.eq(e.currentSlide).outerHeight(!0);
            e.$list.animate({
                height: t
            }, e.options.speed);
        }
    }, r.prototype.animateSlide = function(e, t) {
        var i = {}, o = this;
        o.animateHeight(), !0 === o.options.rtl && !1 === o.options.vertical && (e = -e), 
        !1 === o.transformsEnabled ? !1 === o.options.vertical ? o.$slideTrack.animate({
            left: e
        }, o.options.speed, o.options.easing, t) : o.$slideTrack.animate({
            top: e
        }, o.options.speed, o.options.easing, t) : !1 === o.cssTransitions ? (!0 === o.options.rtl && (o.currentLeft = -o.currentLeft), 
        d({
            animStart: o.currentLeft
        }).animate({
            animStart: e
        }, {
            duration: o.options.speed,
            easing: o.options.easing,
            step: function(e) {
                e = Math.ceil(e), !1 === o.options.vertical ? i[o.animType] = "translate(" + e + "px, 0px)" : i[o.animType] = "translate(0px," + e + "px)", 
                o.$slideTrack.css(i);
            },
            complete: function() {
                t && t.call();
            }
        })) : (o.applyTransition(), e = Math.ceil(e), !1 === o.options.vertical ? i[o.animType] = "translate3d(" + e + "px, 0px, 0px)" : i[o.animType] = "translate3d(0px," + e + "px, 0px)", 
        o.$slideTrack.css(i), t && setTimeout(function() {
            o.disableTransition(), t.call();
        }, o.options.speed));
    }, r.prototype.getNavTarget = function() {
        var e = this.options.asNavFor;
        return e && null !== e && (e = d(e).not(this.$slider)), e;
    }, r.prototype.asNavFor = function(t) {
        var e = this.getNavTarget();
        null !== e && "object" == typeof e && e.each(function() {
            var e = d(this).slick("getSlick");
            e.unslicked || e.slideHandler(t, !0);
        });
    }, r.prototype.applyTransition = function(e) {
        var t = this, i = {};
        !1 === t.options.fade ? i[t.transitionType] = t.transformType + " " + t.options.speed + "ms " + t.options.cssEase : i[t.transitionType] = "opacity " + t.options.speed + "ms " + t.options.cssEase, 
        !1 === t.options.fade ? t.$slideTrack.css(i) : t.$slides.eq(e).css(i);
    }, r.prototype.autoPlay = function() {
        var e = this;
        e.autoPlayClear(), e.slideCount > e.options.slidesToShow && (e.autoPlayTimer = setInterval(e.autoPlayIterator, e.options.autoplaySpeed));
    }, r.prototype.autoPlayClear = function() {
        this.autoPlayTimer && clearInterval(this.autoPlayTimer);
    }, r.prototype.autoPlayIterator = function() {
        var e = this, t = e.currentSlide + e.options.slidesToScroll;
        e.paused || e.interrupted || e.focussed || (!1 === e.options.infinite && (1 === e.direction && e.currentSlide + 1 === e.slideCount - 1 ? e.direction = 0 : 0 === e.direction && (t = e.currentSlide - e.options.slidesToScroll, 
        e.currentSlide - 1 == 0 && (e.direction = 1))), e.slideHandler(t));
    }, r.prototype.buildArrows = function() {
        var e = this;
        !0 === e.options.arrows && (e.$prevArrow = d(e.options.prevArrow).addClass("slick-arrow"), 
        e.$nextArrow = d(e.options.nextArrow).addClass("slick-arrow"), e.slideCount > e.options.slidesToShow ? (e.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), 
        e.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.prependTo(e.options.appendArrows), 
        e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.appendTo(e.options.appendArrows), 
        !0 !== e.options.infinite && e.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true")) : e.$prevArrow.add(e.$nextArrow).addClass("slick-hidden").attr({
            "aria-disabled": "true",
            tabindex: "-1"
        }));
    }, r.prototype.buildDots = function() {
        var e, t, i = this;
        if (!0 === i.options.dots && i.slideCount > i.options.slidesToShow) {
            for (i.$slider.addClass("slick-dotted"), t = d("<ul />").addClass(i.options.dotsClass), 
            e = 0; e <= i.getDotCount(); e += 1) t.append(d("<li />").append(i.options.customPaging.call(this, i, e)));
            i.$dots = t.appendTo(i.options.appendDots), i.$dots.find("li").first().addClass("slick-active").attr("aria-hidden", "false");
        }
    }, r.prototype.buildOut = function() {
        var e = this;
        e.$slides = e.$slider.children(e.options.slide + ":not(.slick-cloned)").addClass("slick-slide"), 
        e.slideCount = e.$slides.length, e.$slides.each(function(e, t) {
            d(t).attr("data-slick-index", e).data("originalStyling", d(t).attr("style") || "");
        }), e.$slider.addClass("slick-slider"), e.$slideTrack = 0 === e.slideCount ? d('<div class="slick-track"/>').appendTo(e.$slider) : e.$slides.wrapAll('<div class="slick-track"/>').parent(), 
        e.$list = e.$slideTrack.wrap('<div aria-live="polite" class="slick-list"/>').parent(), 
        e.$slideTrack.css("opacity", 0), (!0 === e.options.centerMode || !0 === e.options.swipeToSlide) && (e.options.slidesToScroll = 1), 
        d("img[data-lazy]", e.$slider).not("[src]").addClass("slick-loading"), e.setupInfinite(), 
        e.buildArrows(), e.buildDots(), e.updateDots(), e.setSlideClasses("number" == typeof e.currentSlide ? e.currentSlide : 0), 
        !0 === e.options.draggable && e.$list.addClass("draggable");
    }, r.prototype.buildRows = function() {
        var e, t, i, o, s, n, r, a = this;
        if (o = document.createDocumentFragment(), n = a.$slider.children(), 1 < a.options.rows) {
            for (r = a.options.slidesPerRow * a.options.rows, s = Math.ceil(n.length / r), e = 0; e < s; e++) {
                var l = document.createElement("div");
                for (t = 0; t < a.options.rows; t++) {
                    var d = document.createElement("div");
                    for (i = 0; i < a.options.slidesPerRow; i++) {
                        var c = e * r + (t * a.options.slidesPerRow + i);
                        n.get(c) && d.appendChild(n.get(c));
                    }
                    l.appendChild(d);
                }
                o.appendChild(l);
            }
            a.$slider.empty().append(o), a.$slider.children().children().children().css({
                width: 100 / a.options.slidesPerRow + "%",
                display: "inline-block"
            });
        }
    }, r.prototype.checkResponsive = function(e, t) {
        var i, o, s, n = this, r = !1, a = n.$slider.width(), l = window.innerWidth || d(window).width();
        if ("window" === n.respondTo ? s = l : "slider" === n.respondTo ? s = a : "min" === n.respondTo && (s = Math.min(l, a)), 
        n.options.responsive && n.options.responsive.length && null !== n.options.responsive) {
            for (i in o = null, n.breakpoints) n.breakpoints.hasOwnProperty(i) && (!1 === n.originalSettings.mobileFirst ? s < n.breakpoints[i] && (o = n.breakpoints[i]) : s > n.breakpoints[i] && (o = n.breakpoints[i]));
            null !== o ? null !== n.activeBreakpoint ? (o !== n.activeBreakpoint || t) && (n.activeBreakpoint = o, 
            "unslick" === n.breakpointSettings[o] ? n.unslick(o) : (n.options = d.extend({}, n.originalSettings, n.breakpointSettings[o]), 
            !0 === e && (n.currentSlide = n.options.initialSlide), n.refresh(e)), r = o) : (n.activeBreakpoint = o, 
            "unslick" === n.breakpointSettings[o] ? n.unslick(o) : (n.options = d.extend({}, n.originalSettings, n.breakpointSettings[o]), 
            !0 === e && (n.currentSlide = n.options.initialSlide), n.refresh(e)), r = o) : null !== n.activeBreakpoint && (n.activeBreakpoint = null, 
            n.options = n.originalSettings, !0 === e && (n.currentSlide = n.options.initialSlide), 
            n.refresh(e), r = o), e || !1 === r || n.$slider.trigger("breakpoint", [ n, r ]);
        }
    }, r.prototype.changeSlide = function(e, t) {
        var i, o, s = this, n = d(e.currentTarget);
        switch (n.is("a") && e.preventDefault(), n.is("li") || (n = n.closest("li")), i = s.slideCount % s.options.slidesToScroll != 0 ? 0 : (s.slideCount - s.currentSlide) % s.options.slidesToScroll, 
        e.data.message) {
          case "previous":
            o = 0 === i ? s.options.slidesToScroll : s.options.slidesToShow - i, s.slideCount > s.options.slidesToShow && s.slideHandler(s.currentSlide - o, !1, t);
            break;

          case "next":
            o = 0 === i ? s.options.slidesToScroll : i, s.slideCount > s.options.slidesToShow && s.slideHandler(s.currentSlide + o, !1, t);
            break;

          case "index":
            var r = 0 === e.data.index ? 0 : e.data.index || n.index() * s.options.slidesToScroll;
            s.slideHandler(s.checkNavigable(r), !1, t), n.children().trigger("focus");
            break;

          default:
            return;
        }
    }, r.prototype.checkNavigable = function(e) {
        var t, i;
        if (i = 0, e > (t = this.getNavigableIndexes())[t.length - 1]) e = t[t.length - 1]; else for (var o in t) {
            if (e < t[o]) {
                e = i;
                break;
            }
            i = t[o];
        }
        return e;
    }, r.prototype.cleanUpEvents = function() {
        var e = this;
        e.options.dots && null !== e.$dots && d("li", e.$dots).off("click.slick", e.changeSlide).off("mouseenter.slick", d.proxy(e.interrupt, e, !0)).off("mouseleave.slick", d.proxy(e.interrupt, e, !1)), 
        e.$slider.off("focus.slick blur.slick"), !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow && e.$prevArrow.off("click.slick", e.changeSlide), 
        e.$nextArrow && e.$nextArrow.off("click.slick", e.changeSlide)), e.$list.off("touchstart.slick mousedown.slick", e.swipeHandler), 
        e.$list.off("touchmove.slick mousemove.slick", e.swipeHandler), e.$list.off("touchend.slick mouseup.slick", e.swipeHandler), 
        e.$list.off("touchcancel.slick mouseleave.slick", e.swipeHandler), e.$list.off("click.slick", e.clickHandler), 
        d(document).off(e.visibilityChange, e.visibility), e.cleanUpSlideEvents(), !0 === e.options.accessibility && e.$list.off("keydown.slick", e.keyHandler), 
        !0 === e.options.focusOnSelect && d(e.$slideTrack).children().off("click.slick", e.selectHandler), 
        d(window).off("orientationchange.slick.slick-" + e.instanceUid, e.orientationChange), 
        d(window).off("resize.slick.slick-" + e.instanceUid, e.resize), d("[draggable!=true]", e.$slideTrack).off("dragstart", e.preventDefault), 
        d(window).off("load.slick.slick-" + e.instanceUid, e.setPosition), d(document).off("ready.slick.slick-" + e.instanceUid, e.setPosition);
    }, r.prototype.cleanUpSlideEvents = function() {
        var e = this;
        e.$list.off("mouseenter.slick", d.proxy(e.interrupt, e, !0)), e.$list.off("mouseleave.slick", d.proxy(e.interrupt, e, !1));
    }, r.prototype.cleanUpRows = function() {
        var e;
        1 < this.options.rows && ((e = this.$slides.children().children()).removeAttr("style"), 
        this.$slider.empty().append(e));
    }, r.prototype.clickHandler = function(e) {
        !1 === this.shouldClick && (e.stopImmediatePropagation(), e.stopPropagation(), e.preventDefault());
    }, r.prototype.destroy = function(e) {
        var t = this;
        t.autoPlayClear(), t.touchObject = {}, t.cleanUpEvents(), d(".slick-cloned", t.$slider).detach(), 
        t.$dots && t.$dots.remove(), t.$prevArrow && t.$prevArrow.length && (t.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), 
        t.htmlExpr.test(t.options.prevArrow) && t.$prevArrow.remove()), t.$nextArrow && t.$nextArrow.length && (t.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), 
        t.htmlExpr.test(t.options.nextArrow) && t.$nextArrow.remove()), t.$slides && (t.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function() {
            d(this).attr("style", d(this).data("originalStyling"));
        }), t.$slideTrack.children(this.options.slide).detach(), t.$slideTrack.detach(), 
        t.$list.detach(), t.$slider.append(t.$slides)), t.cleanUpRows(), t.$slider.removeClass("slick-slider"), 
        t.$slider.removeClass("slick-initialized"), t.$slider.removeClass("slick-dotted"), 
        t.unslicked = !0, e || t.$slider.trigger("destroy", [ t ]);
    }, r.prototype.disableTransition = function(e) {
        var t = {};
        t[this.transitionType] = "", !1 === this.options.fade ? this.$slideTrack.css(t) : this.$slides.eq(e).css(t);
    }, r.prototype.fadeSlide = function(e, t) {
        var i = this;
        !1 === i.cssTransitions ? (i.$slides.eq(e).css({
            zIndex: i.options.zIndex
        }), i.$slides.eq(e).animate({
            opacity: 1
        }, i.options.speed, i.options.easing, t)) : (i.applyTransition(e), i.$slides.eq(e).css({
            opacity: 1,
            zIndex: i.options.zIndex
        }), t && setTimeout(function() {
            i.disableTransition(e), t.call();
        }, i.options.speed));
    }, r.prototype.fadeSlideOut = function(e) {
        var t = this;
        !1 === t.cssTransitions ? t.$slides.eq(e).animate({
            opacity: 0,
            zIndex: t.options.zIndex - 2
        }, t.options.speed, t.options.easing) : (t.applyTransition(e), t.$slides.eq(e).css({
            opacity: 0,
            zIndex: t.options.zIndex - 2
        }));
    }, r.prototype.filterSlides = r.prototype.slickFilter = function(e) {
        var t = this;
        null !== e && (t.$slidesCache = t.$slides, t.unload(), t.$slideTrack.children(this.options.slide).detach(), 
        t.$slidesCache.filter(e).appendTo(t.$slideTrack), t.reinit());
    }, r.prototype.focusHandler = function() {
        var i = this;
        i.$slider.off("focus.slick blur.slick").on("focus.slick blur.slick", "*:not(.slick-arrow)", function(e) {
            e.stopImmediatePropagation();
            var t = d(this);
            setTimeout(function() {
                i.options.pauseOnFocus && (i.focussed = t.is(":focus"), i.autoPlay());
            }, 0);
        });
    }, r.prototype.getCurrent = r.prototype.slickCurrentSlide = function() {
        return this.currentSlide;
    }, r.prototype.getDotCount = function() {
        var e = this, t = 0, i = 0, o = 0;
        if (!0 === e.options.infinite) for (;t < e.slideCount; ) ++o, t = i + e.options.slidesToScroll, 
        i += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow; else if (!0 === e.options.centerMode) o = e.slideCount; else if (e.options.asNavFor) for (;t < e.slideCount; ) ++o, 
        t = i + e.options.slidesToScroll, i += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow; else o = 1 + Math.ceil((e.slideCount - e.options.slidesToShow) / e.options.slidesToScroll);
        return o - 1;
    }, r.prototype.getLeft = function(e) {
        var t, i, o, s = this, n = 0;
        return s.slideOffset = 0, i = s.$slides.first().outerHeight(!0), !0 === s.options.infinite ? (s.slideCount > s.options.slidesToShow && (s.slideOffset = s.slideWidth * s.options.slidesToShow * -1, 
        n = i * s.options.slidesToShow * -1), s.slideCount % s.options.slidesToScroll != 0 && e + s.options.slidesToScroll > s.slideCount && s.slideCount > s.options.slidesToShow && (e > s.slideCount ? (s.slideOffset = (s.options.slidesToShow - (e - s.slideCount)) * s.slideWidth * -1, 
        n = (s.options.slidesToShow - (e - s.slideCount)) * i * -1) : (s.slideOffset = s.slideCount % s.options.slidesToScroll * s.slideWidth * -1, 
        n = s.slideCount % s.options.slidesToScroll * i * -1))) : e + s.options.slidesToShow > s.slideCount && (s.slideOffset = (e + s.options.slidesToShow - s.slideCount) * s.slideWidth, 
        n = (e + s.options.slidesToShow - s.slideCount) * i), s.slideCount <= s.options.slidesToShow && (n = s.slideOffset = 0), 
        !0 === s.options.centerMode && !0 === s.options.infinite ? s.slideOffset += s.slideWidth * Math.floor(s.options.slidesToShow / 2) - s.slideWidth : !0 === s.options.centerMode && (s.slideOffset = 0, 
        s.slideOffset += s.slideWidth * Math.floor(s.options.slidesToShow / 2)), t = !1 === s.options.vertical ? e * s.slideWidth * -1 + s.slideOffset : e * i * -1 + n, 
        !0 === s.options.variableWidth && (o = s.slideCount <= s.options.slidesToShow || !1 === s.options.infinite ? s.$slideTrack.children(".slick-slide").eq(e) : s.$slideTrack.children(".slick-slide").eq(e + s.options.slidesToShow), 
        t = !0 === s.options.rtl ? o[0] ? -1 * (s.$slideTrack.width() - o[0].offsetLeft - o.width()) : 0 : o[0] ? -1 * o[0].offsetLeft : 0, 
        !0 === s.options.centerMode && (o = s.slideCount <= s.options.slidesToShow || !1 === s.options.infinite ? s.$slideTrack.children(".slick-slide").eq(e) : s.$slideTrack.children(".slick-slide").eq(e + s.options.slidesToShow + 1), 
        t = !0 === s.options.rtl ? o[0] ? -1 * (s.$slideTrack.width() - o[0].offsetLeft - o.width()) : 0 : o[0] ? -1 * o[0].offsetLeft : 0, 
        t += (s.$list.width() - o.outerWidth()) / 2)), t;
    }, r.prototype.getOption = r.prototype.slickGetOption = function(e) {
        return this.options[e];
    }, r.prototype.getNavigableIndexes = function() {
        var e, t = this, i = 0, o = 0, s = [];
        for (!1 === t.options.infinite ? e = t.slideCount : (i = -1 * t.options.slidesToScroll, 
        o = -1 * t.options.slidesToScroll, e = 2 * t.slideCount); i < e; ) s.push(i), i = o + t.options.slidesToScroll, 
        o += t.options.slidesToScroll <= t.options.slidesToShow ? t.options.slidesToScroll : t.options.slidesToShow;
        return s;
    }, r.prototype.getSlick = function() {
        return this;
    }, r.prototype.getSlideCount = function() {
        var i, o, s = this;
        return o = !0 === s.options.centerMode ? s.slideWidth * Math.floor(s.options.slidesToShow / 2) : 0, 
        !0 === s.options.swipeToSlide ? (s.$slideTrack.find(".slick-slide").each(function(e, t) {
            return t.offsetLeft - o + d(t).outerWidth() / 2 > -1 * s.swipeLeft ? (i = t, !1) : void 0;
        }), Math.abs(d(i).attr("data-slick-index") - s.currentSlide) || 1) : s.options.slidesToScroll;
    }, r.prototype.goTo = r.prototype.slickGoTo = function(e, t) {
        this.changeSlide({
            data: {
                message: "index",
                index: parseInt(e)
            }
        }, t);
    }, r.prototype.init = function(e) {
        var t = this;
        d(t.$slider).hasClass("slick-initialized") || (d(t.$slider).addClass("slick-initialized"), 
        t.buildRows(), t.buildOut(), t.setProps(), t.startLoad(), t.loadSlider(), t.initializeEvents(), 
        t.updateArrows(), t.updateDots(), t.checkResponsive(!0), t.focusHandler()), e && t.$slider.trigger("init", [ t ]), 
        !0 === t.options.accessibility && t.initADA(), t.options.autoplay && (t.paused = !1, 
        t.autoPlay());
    }, r.prototype.initADA = function() {
        var t = this;
        t.$slides.add(t.$slideTrack.find(".slick-cloned")).attr({
            "aria-hidden": "true",
            tabindex: "-1"
        }).find("a, input, button, select").attr({
            tabindex: "-1"
        }), t.$slideTrack.attr("role", "listbox"), t.$slides.not(t.$slideTrack.find(".slick-cloned")).each(function(e) {
            d(this).attr({
                role: "option",
                "aria-describedby": "slick-slide" + t.instanceUid + e
            });
        }), null !== t.$dots && t.$dots.attr("role", "tablist").find("li").each(function(e) {
            d(this).attr({
                role: "presentation",
                "aria-selected": "false",
                "aria-controls": "navigation" + t.instanceUid + e,
                id: "slick-slide" + t.instanceUid + e
            });
        }).first().attr("aria-selected", "true").end().find("button").attr("role", "button").end().closest("div").attr("role", "toolbar"), 
        t.activateADA();
    }, r.prototype.initArrowEvents = function() {
        var e = this;
        !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow.off("click.slick").on("click.slick", {
            message: "previous"
        }, e.changeSlide), e.$nextArrow.off("click.slick").on("click.slick", {
            message: "next"
        }, e.changeSlide));
    }, r.prototype.initDotEvents = function() {
        var e = this;
        !0 === e.options.dots && e.slideCount > e.options.slidesToShow && d("li", e.$dots).on("click.slick", {
            message: "index"
        }, e.changeSlide), !0 === e.options.dots && !0 === e.options.pauseOnDotsHover && d("li", e.$dots).on("mouseenter.slick", d.proxy(e.interrupt, e, !0)).on("mouseleave.slick", d.proxy(e.interrupt, e, !1));
    }, r.prototype.initSlideEvents = function() {
        var e = this;
        e.options.pauseOnHover && (e.$list.on("mouseenter.slick", d.proxy(e.interrupt, e, !0)), 
        e.$list.on("mouseleave.slick", d.proxy(e.interrupt, e, !1)));
    }, r.prototype.initializeEvents = function() {
        var e = this;
        e.initArrowEvents(), e.initDotEvents(), e.initSlideEvents(), e.$list.on("touchstart.slick mousedown.slick", {
            action: "start"
        }, e.swipeHandler), e.$list.on("touchmove.slick mousemove.slick", {
            action: "move"
        }, e.swipeHandler), e.$list.on("touchend.slick mouseup.slick", {
            action: "end"
        }, e.swipeHandler), e.$list.on("touchcancel.slick mouseleave.slick", {
            action: "end"
        }, e.swipeHandler), e.$list.on("click.slick", e.clickHandler), d(document).on(e.visibilityChange, d.proxy(e.visibility, e)), 
        !0 === e.options.accessibility && e.$list.on("keydown.slick", e.keyHandler), !0 === e.options.focusOnSelect && d(e.$slideTrack).children().on("click.slick", e.selectHandler), 
        d(window).on("orientationchange.slick.slick-" + e.instanceUid, d.proxy(e.orientationChange, e)), 
        d(window).on("resize.slick.slick-" + e.instanceUid, d.proxy(e.resize, e)), d("[draggable!=true]", e.$slideTrack).on("dragstart", e.preventDefault), 
        d(window).on("load.slick.slick-" + e.instanceUid, e.setPosition), d(document).on("ready.slick.slick-" + e.instanceUid, e.setPosition);
    }, r.prototype.initUI = function() {
        var e = this;
        !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow.show(), 
        e.$nextArrow.show()), !0 === e.options.dots && e.slideCount > e.options.slidesToShow && e.$dots.show();
    }, r.prototype.keyHandler = function(e) {
        var t = this;
        e.target.tagName.match("TEXTAREA|INPUT|SELECT") || (37 === e.keyCode && !0 === t.options.accessibility ? t.changeSlide({
            data: {
                message: !0 === t.options.rtl ? "next" : "previous"
            }
        }) : 39 === e.keyCode && !0 === t.options.accessibility && t.changeSlide({
            data: {
                message: !0 === t.options.rtl ? "previous" : "next"
            }
        }));
    }, r.prototype.lazyLoad = function() {
        function e(e) {
            d("img[data-lazy]", e).each(function() {
                var e = d(this), t = d(this).attr("data-lazy"), i = document.createElement("img");
                i.onload = function() {
                    e.animate({
                        opacity: 0
                    }, 100, function() {
                        e.attr("src", t).animate({
                            opacity: 1
                        }, 200, function() {
                            e.removeAttr("data-lazy").removeClass("slick-loading");
                        }), o.$slider.trigger("lazyLoaded", [ o, e, t ]);
                    });
                }, i.onerror = function() {
                    e.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), 
                    o.$slider.trigger("lazyLoadError", [ o, e, t ]);
                }, i.src = t;
            });
        }
        var t, i, o = this;
        !0 === o.options.centerMode ? !0 === o.options.infinite ? i = (t = o.currentSlide + (o.options.slidesToShow / 2 + 1)) + o.options.slidesToShow + 2 : (t = Math.max(0, o.currentSlide - (o.options.slidesToShow / 2 + 1)), 
        i = o.options.slidesToShow / 2 + 1 + 2 + o.currentSlide) : (t = o.options.infinite ? o.options.slidesToShow + o.currentSlide : o.currentSlide, 
        i = Math.ceil(t + o.options.slidesToShow), !0 === o.options.fade && (0 < t && t--, 
        i <= o.slideCount && i++)), e(o.$slider.find(".slick-slide").slice(t, i)), o.slideCount <= o.options.slidesToShow ? e(o.$slider.find(".slick-slide")) : o.currentSlide >= o.slideCount - o.options.slidesToShow ? e(o.$slider.find(".slick-cloned").slice(0, o.options.slidesToShow)) : 0 === o.currentSlide && e(o.$slider.find(".slick-cloned").slice(-1 * o.options.slidesToShow));
    }, r.prototype.loadSlider = function() {
        var e = this;
        e.setPosition(), e.$slideTrack.css({
            opacity: 1
        }), e.$slider.removeClass("slick-loading"), e.initUI(), "progressive" === e.options.lazyLoad && e.progressiveLazyLoad();
    }, r.prototype.next = r.prototype.slickNext = function() {
        this.changeSlide({
            data: {
                message: "next"
            }
        });
    }, r.prototype.orientationChange = function() {
        this.checkResponsive(), this.setPosition();
    }, r.prototype.pause = r.prototype.slickPause = function() {
        this.autoPlayClear(), this.paused = !0;
    }, r.prototype.play = r.prototype.slickPlay = function() {
        var e = this;
        e.autoPlay(), e.options.autoplay = !0, e.paused = !1, e.focussed = !1, e.interrupted = !1;
    }, r.prototype.postSlide = function(e) {
        var t = this;
        t.unslicked || (t.$slider.trigger("afterChange", [ t, e ]), t.animating = !1, t.setPosition(), 
        t.swipeLeft = null, t.options.autoplay && t.autoPlay(), !0 === t.options.accessibility && t.initADA());
    }, r.prototype.prev = r.prototype.slickPrev = function() {
        this.changeSlide({
            data: {
                message: "previous"
            }
        });
    }, r.prototype.preventDefault = function(e) {
        e.preventDefault();
    }, r.prototype.progressiveLazyLoad = function(e) {
        e = e || 1;
        var t, i, o, s = this, n = d("img[data-lazy]", s.$slider);
        n.length ? (t = n.first(), i = t.attr("data-lazy"), (o = document.createElement("img")).onload = function() {
            t.attr("src", i).removeAttr("data-lazy").removeClass("slick-loading"), !0 === s.options.adaptiveHeight && s.setPosition(), 
            s.$slider.trigger("lazyLoaded", [ s, t, i ]), s.progressiveLazyLoad();
        }, o.onerror = function() {
            e < 3 ? setTimeout(function() {
                s.progressiveLazyLoad(e + 1);
            }, 500) : (t.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), 
            s.$slider.trigger("lazyLoadError", [ s, t, i ]), s.progressiveLazyLoad());
        }, o.src = i) : s.$slider.trigger("allImagesLoaded", [ s ]);
    }, r.prototype.refresh = function(e) {
        var t, i, o = this;
        i = o.slideCount - o.options.slidesToShow, !o.options.infinite && o.currentSlide > i && (o.currentSlide = i), 
        o.slideCount <= o.options.slidesToShow && (o.currentSlide = 0), t = o.currentSlide, 
        o.destroy(!0), d.extend(o, o.initials, {
            currentSlide: t
        }), o.init(), e || o.changeSlide({
            data: {
                message: "index",
                index: t
            }
        }, !1);
    }, r.prototype.registerBreakpoints = function() {
        var e, t, i, o = this, s = o.options.responsive || null;
        if ("array" === d.type(s) && s.length) {
            for (e in o.respondTo = o.options.respondTo || "window", s) if (i = o.breakpoints.length - 1, 
            t = s[e].breakpoint, s.hasOwnProperty(e)) {
                for (;0 <= i; ) o.breakpoints[i] && o.breakpoints[i] === t && o.breakpoints.splice(i, 1), 
                i--;
                o.breakpoints.push(t), o.breakpointSettings[t] = s[e].settings;
            }
            o.breakpoints.sort(function(e, t) {
                return o.options.mobileFirst ? e - t : t - e;
            });
        }
    }, r.prototype.reinit = function() {
        var e = this;
        e.$slides = e.$slideTrack.children(e.options.slide).addClass("slick-slide"), e.slideCount = e.$slides.length, 
        e.currentSlide >= e.slideCount && 0 !== e.currentSlide && (e.currentSlide = e.currentSlide - e.options.slidesToScroll), 
        e.slideCount <= e.options.slidesToShow && (e.currentSlide = 0), e.registerBreakpoints(), 
        e.setProps(), e.setupInfinite(), e.buildArrows(), e.updateArrows(), e.initArrowEvents(), 
        e.buildDots(), e.updateDots(), e.initDotEvents(), e.cleanUpSlideEvents(), e.initSlideEvents(), 
        e.checkResponsive(!1, !0), !0 === e.options.focusOnSelect && d(e.$slideTrack).children().on("click.slick", e.selectHandler), 
        e.setSlideClasses("number" == typeof e.currentSlide ? e.currentSlide : 0), e.setPosition(), 
        e.focusHandler(), e.paused = !e.options.autoplay, e.autoPlay(), e.$slider.trigger("reInit", [ e ]);
    }, r.prototype.resize = function() {
        var e = this;
        d(window).width() !== e.windowWidth && (clearTimeout(e.windowDelay), e.windowDelay = window.setTimeout(function() {
            e.windowWidth = d(window).width(), e.checkResponsive(), e.unslicked || e.setPosition();
        }, 50));
    }, r.prototype.removeSlide = r.prototype.slickRemove = function(e, t, i) {
        var o = this;
        return "boolean" == typeof e ? e = !0 === (t = e) ? 0 : o.slideCount - 1 : e = !0 === t ? --e : e, 
        !(o.slideCount < 1 || e < 0 || e > o.slideCount - 1) && (o.unload(), !0 === i ? o.$slideTrack.children().remove() : o.$slideTrack.children(this.options.slide).eq(e).remove(), 
        o.$slides = o.$slideTrack.children(this.options.slide), o.$slideTrack.children(this.options.slide).detach(), 
        o.$slideTrack.append(o.$slides), o.$slidesCache = o.$slides, void o.reinit());
    }, r.prototype.setCSS = function(e) {
        var t, i, o = this, s = {};
        !0 === o.options.rtl && (e = -e), t = "left" == o.positionProp ? Math.ceil(e) + "px" : "0px", 
        i = "top" == o.positionProp ? Math.ceil(e) + "px" : "0px", s[o.positionProp] = e, 
        !1 === o.transformsEnabled || (!(s = {}) === o.cssTransitions ? s[o.animType] = "translate(" + t + ", " + i + ")" : s[o.animType] = "translate3d(" + t + ", " + i + ", 0px)"), 
        o.$slideTrack.css(s);
    }, r.prototype.setDimensions = function() {
        var e = this;
        !1 === e.options.vertical ? !0 === e.options.centerMode && e.$list.css({
            padding: "0px " + e.options.centerPadding
        }) : (e.$list.height(e.$slides.first().outerHeight(!0) * e.options.slidesToShow), 
        !0 === e.options.centerMode && e.$list.css({
            padding: e.options.centerPadding + " 0px"
        })), e.listWidth = e.$list.width(), e.listHeight = e.$list.height(), !1 === e.options.vertical && !1 === e.options.variableWidth ? (e.slideWidth = Math.ceil(e.listWidth / e.options.slidesToShow), 
        e.$slideTrack.width(Math.ceil(e.slideWidth * e.$slideTrack.children(".slick-slide").length))) : !0 === e.options.variableWidth ? e.$slideTrack.width(5e3 * e.slideCount) : (e.slideWidth = Math.ceil(e.listWidth), 
        e.$slideTrack.height(Math.ceil(e.$slides.first().outerHeight(!0) * e.$slideTrack.children(".slick-slide").length)));
        var t = e.$slides.first().outerWidth(!0) - e.$slides.first().width();
        !1 === e.options.variableWidth && e.$slideTrack.children(".slick-slide").width(e.slideWidth - t);
    }, r.prototype.setFade = function() {
        var i, o = this;
        o.$slides.each(function(e, t) {
            i = o.slideWidth * e * -1, !0 === o.options.rtl ? d(t).css({
                position: "relative",
                right: i,
                top: 0,
                zIndex: o.options.zIndex - 2,
                opacity: 0
            }) : d(t).css({
                position: "relative",
                left: i,
                top: 0,
                zIndex: o.options.zIndex - 2,
                opacity: 0
            });
        }), o.$slides.eq(o.currentSlide).css({
            zIndex: o.options.zIndex - 1,
            opacity: 1
        });
    }, r.prototype.setHeight = function() {
        var e = this;
        if (1 === e.options.slidesToShow && !0 === e.options.adaptiveHeight && !1 === e.options.vertical) {
            var t = e.$slides.eq(e.currentSlide).outerHeight(!0);
            e.$list.css("height", t);
        }
    }, r.prototype.setOption = r.prototype.slickSetOption = function() {
        var e, t, i, o, s, n = this, r = !1;
        if ("object" === d.type(arguments[0]) ? (i = arguments[0], r = arguments[1], s = "multiple") : "string" === d.type(arguments[0]) && (i = arguments[0], 
        o = arguments[1], r = arguments[2], "responsive" === arguments[0] && "array" === d.type(arguments[1]) ? s = "responsive" : void 0 !== arguments[1] && (s = "single")), 
        "single" === s) n.options[i] = o; else if ("multiple" === s) d.each(i, function(e, t) {
            n.options[e] = t;
        }); else if ("responsive" === s) for (t in o) if ("array" !== d.type(n.options.responsive)) n.options.responsive = [ o[t] ]; else {
            for (e = n.options.responsive.length - 1; 0 <= e; ) n.options.responsive[e].breakpoint === o[t].breakpoint && n.options.responsive.splice(e, 1), 
            e--;
            n.options.responsive.push(o[t]);
        }
        r && (n.unload(), n.reinit());
    }, r.prototype.setPosition = function() {
        var e = this;
        e.setDimensions(), e.setHeight(), !1 === e.options.fade ? e.setCSS(e.getLeft(e.currentSlide)) : e.setFade(), 
        e.$slider.trigger("setPosition", [ e ]);
    }, r.prototype.setProps = function() {
        var e = this, t = document.body.style;
        e.positionProp = !0 === e.options.vertical ? "top" : "left", "top" === e.positionProp ? e.$slider.addClass("slick-vertical") : e.$slider.removeClass("slick-vertical"), 
        (void 0 !== t.WebkitTransition || void 0 !== t.MozTransition || void 0 !== t.msTransition) && !0 === e.options.useCSS && (e.cssTransitions = !0), 
        e.options.fade && ("number" == typeof e.options.zIndex ? e.options.zIndex < 3 && (e.options.zIndex = 3) : e.options.zIndex = e.defaults.zIndex), 
        void 0 !== t.OTransform && (e.animType = "OTransform", e.transformType = "-o-transform", 
        e.transitionType = "OTransition", void 0 === t.perspectiveProperty && void 0 === t.webkitPerspective && (e.animType = !1)), 
        void 0 !== t.MozTransform && (e.animType = "MozTransform", e.transformType = "-moz-transform", 
        e.transitionType = "MozTransition", void 0 === t.perspectiveProperty && void 0 === t.MozPerspective && (e.animType = !1)), 
        void 0 !== t.webkitTransform && (e.animType = "webkitTransform", e.transformType = "-webkit-transform", 
        e.transitionType = "webkitTransition", void 0 === t.perspectiveProperty && void 0 === t.webkitPerspective && (e.animType = !1)), 
        void 0 !== t.msTransform && (e.animType = "msTransform", e.transformType = "-ms-transform", 
        e.transitionType = "msTransition", void 0 === t.msTransform && (e.animType = !1)), 
        void 0 !== t.transform && !1 !== e.animType && (e.animType = "transform", e.transformType = "transform", 
        e.transitionType = "transition"), e.transformsEnabled = e.options.useTransform && null !== e.animType && !1 !== e.animType;
    }, r.prototype.setSlideClasses = function(e) {
        var t, i, o, s, n = this;
        i = n.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden", "true"), 
        n.$slides.eq(e).addClass("slick-current"), !0 === n.options.centerMode ? (t = Math.floor(n.options.slidesToShow / 2), 
        !0 === n.options.infinite && (t <= e && e <= n.slideCount - 1 - t ? n.$slides.slice(e - t, e + t + 1).addClass("slick-active").attr("aria-hidden", "false") : (o = n.options.slidesToShow + e, 
        i.slice(o - t + 1, o + t + 2).addClass("slick-active").attr("aria-hidden", "false")), 
        0 === e ? i.eq(i.length - 1 - n.options.slidesToShow).addClass("slick-center") : e === n.slideCount - 1 && i.eq(n.options.slidesToShow).addClass("slick-center")), 
        n.$slides.eq(e).addClass("slick-center")) : 0 <= e && e <= n.slideCount - n.options.slidesToShow ? n.$slides.slice(e, e + n.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false") : i.length <= n.options.slidesToShow ? i.addClass("slick-active").attr("aria-hidden", "false") : (s = n.slideCount % n.options.slidesToShow, 
        o = !0 === n.options.infinite ? n.options.slidesToShow + e : e, n.options.slidesToShow == n.options.slidesToScroll && n.slideCount - e < n.options.slidesToShow ? i.slice(o - (n.options.slidesToShow - s), o + s).addClass("slick-active").attr("aria-hidden", "false") : i.slice(o, o + n.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false")), 
        "ondemand" === n.options.lazyLoad && n.lazyLoad();
    }, r.prototype.setupInfinite = function() {
        var e, t, i, o = this;
        if (!0 === o.options.fade && (o.options.centerMode = !1), !0 === o.options.infinite && !1 === o.options.fade && (t = null, 
        o.slideCount > o.options.slidesToShow)) {
            for (i = !0 === o.options.centerMode ? o.options.slidesToShow + 1 : o.options.slidesToShow, 
            e = o.slideCount; e > o.slideCount - i; e -= 1) t = e - 1, d(o.$slides[t]).clone(!0).attr("id", "").attr("data-slick-index", t - o.slideCount).prependTo(o.$slideTrack).addClass("slick-cloned");
            for (e = 0; e < i; e += 1) t = e, d(o.$slides[t]).clone(!0).attr("id", "").attr("data-slick-index", t + o.slideCount).appendTo(o.$slideTrack).addClass("slick-cloned");
            o.$slideTrack.find(".slick-cloned").find("[id]").each(function() {
                d(this).attr("id", "");
            });
        }
    }, r.prototype.interrupt = function(e) {
        e || this.autoPlay(), this.interrupted = e;
    }, r.prototype.selectHandler = function(e) {
        var t = this, i = d(e.target).is(".slick-slide") ? d(e.target) : d(e.target).parents(".slick-slide"), o = parseInt(i.attr("data-slick-index"));
        return o || (o = 0), t.slideCount <= t.options.slidesToShow ? (t.setSlideClasses(o), 
        void t.asNavFor(o)) : void t.slideHandler(o);
    }, r.prototype.slideHandler = function(e, t, i) {
        var o, s, n, r, a, l = null, d = this;
        return t = t || !1, !0 === d.animating && !0 === d.options.waitForAnimate || !0 === d.options.fade && d.currentSlide === e || d.slideCount <= d.options.slidesToShow ? void 0 : (!1 === t && d.asNavFor(e), 
        o = e, l = d.getLeft(o), r = d.getLeft(d.currentSlide), d.currentLeft = null === d.swipeLeft ? r : d.swipeLeft, 
        !1 === d.options.infinite && !1 === d.options.centerMode && (e < 0 || e > d.getDotCount() * d.options.slidesToScroll) ? void (!1 === d.options.fade && (o = d.currentSlide, 
        !0 !== i ? d.animateSlide(r, function() {
            d.postSlide(o);
        }) : d.postSlide(o))) : !1 === d.options.infinite && !0 === d.options.centerMode && (e < 0 || e > d.slideCount - d.options.slidesToScroll) ? void (!1 === d.options.fade && (o = d.currentSlide, 
        !0 !== i ? d.animateSlide(r, function() {
            d.postSlide(o);
        }) : d.postSlide(o))) : (d.options.autoplay && clearInterval(d.autoPlayTimer), s = o < 0 ? d.slideCount % d.options.slidesToScroll != 0 ? d.slideCount - d.slideCount % d.options.slidesToScroll : d.slideCount + o : o >= d.slideCount ? d.slideCount % d.options.slidesToScroll != 0 ? 0 : o - d.slideCount : o, 
        d.animating = !0, d.$slider.trigger("beforeChange", [ d, d.currentSlide, s ]), n = d.currentSlide, 
        d.currentSlide = s, d.setSlideClasses(d.currentSlide), d.options.asNavFor && ((a = (a = d.getNavTarget()).slick("getSlick")).slideCount <= a.options.slidesToShow && a.setSlideClasses(d.currentSlide)), 
        d.updateDots(), d.updateArrows(), !0 === d.options.fade ? (!0 !== i ? (d.fadeSlideOut(n), 
        d.fadeSlide(s, function() {
            d.postSlide(s);
        })) : d.postSlide(s), void d.animateHeight()) : void (!0 !== i ? d.animateSlide(l, function() {
            d.postSlide(s);
        }) : d.postSlide(s))));
    }, r.prototype.startLoad = function() {
        var e = this;
        !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow.hide(), 
        e.$nextArrow.hide()), !0 === e.options.dots && e.slideCount > e.options.slidesToShow && e.$dots.hide(), 
        e.$slider.addClass("slick-loading");
    }, r.prototype.swipeDirection = function() {
        var e, t, i, o, s = this;
        return e = s.touchObject.startX - s.touchObject.curX, t = s.touchObject.startY - s.touchObject.curY, 
        i = Math.atan2(t, e), (o = Math.round(180 * i / Math.PI)) < 0 && (o = 360 - Math.abs(o)), 
        o <= 45 && 0 <= o ? !1 === s.options.rtl ? "left" : "right" : o <= 360 && 315 <= o ? !1 === s.options.rtl ? "left" : "right" : 135 <= o && o <= 225 ? !1 === s.options.rtl ? "right" : "left" : !0 === s.options.verticalSwiping ? 35 <= o && o <= 135 ? "down" : "up" : "vertical";
    }, r.prototype.swipeEnd = function(e) {
        var t, i, o = this;
        if (o.dragging = !1, o.interrupted = !1, o.shouldClick = !(10 < o.touchObject.swipeLength), 
        void 0 === o.touchObject.curX) return !1;
        if (!0 === o.touchObject.edgeHit && o.$slider.trigger("edge", [ o, o.swipeDirection() ]), 
        o.touchObject.swipeLength >= o.touchObject.minSwipe) {
            switch (i = o.swipeDirection()) {
              case "left":
              case "down":
                t = o.options.swipeToSlide ? o.checkNavigable(o.currentSlide + o.getSlideCount()) : o.currentSlide + o.getSlideCount(), 
                o.currentDirection = 0;
                break;

              case "right":
              case "up":
                t = o.options.swipeToSlide ? o.checkNavigable(o.currentSlide - o.getSlideCount()) : o.currentSlide - o.getSlideCount(), 
                o.currentDirection = 1;
            }
            "vertical" != i && (o.slideHandler(t), o.touchObject = {}, o.$slider.trigger("swipe", [ o, i ]));
        } else o.touchObject.startX !== o.touchObject.curX && (o.slideHandler(o.currentSlide), 
        o.touchObject = {});
    }, r.prototype.swipeHandler = function(e) {
        var t = this;
        if (!(!1 === t.options.swipe || "ontouchend" in document && !1 === t.options.swipe || !1 === t.options.draggable && -1 !== e.type.indexOf("mouse"))) switch (t.touchObject.fingerCount = e.originalEvent && void 0 !== e.originalEvent.touches ? e.originalEvent.touches.length : 1, 
        t.touchObject.minSwipe = t.listWidth / t.options.touchThreshold, !0 === t.options.verticalSwiping && (t.touchObject.minSwipe = t.listHeight / t.options.touchThreshold), 
        e.data.action) {
          case "start":
            t.swipeStart(e);
            break;

          case "move":
            t.swipeMove(e);
            break;

          case "end":
            t.swipeEnd(e);
        }
    }, r.prototype.swipeMove = function(e) {
        var t, i, o, s, n, r = this;
        return n = void 0 !== e.originalEvent ? e.originalEvent.touches : null, !(!r.dragging || n && 1 !== n.length) && (t = r.getLeft(r.currentSlide), 
        r.touchObject.curX = void 0 !== n ? n[0].pageX : e.clientX, r.touchObject.curY = void 0 !== n ? n[0].pageY : e.clientY, 
        r.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(r.touchObject.curX - r.touchObject.startX, 2))), 
        !0 === r.options.verticalSwiping && (r.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(r.touchObject.curY - r.touchObject.startY, 2)))), 
        "vertical" !== (i = r.swipeDirection()) ? (void 0 !== e.originalEvent && 4 < r.touchObject.swipeLength && e.preventDefault(), 
        s = (!1 === r.options.rtl ? 1 : -1) * (r.touchObject.curX > r.touchObject.startX ? 1 : -1), 
        !0 === r.options.verticalSwiping && (s = r.touchObject.curY > r.touchObject.startY ? 1 : -1), 
        o = r.touchObject.swipeLength, (r.touchObject.edgeHit = !1) === r.options.infinite && (0 === r.currentSlide && "right" === i || r.currentSlide >= r.getDotCount() && "left" === i) && (o = r.touchObject.swipeLength * r.options.edgeFriction, 
        r.touchObject.edgeHit = !0), !1 === r.options.vertical ? r.swipeLeft = t + o * s : r.swipeLeft = t + o * (r.$list.height() / r.listWidth) * s, 
        !0 === r.options.verticalSwiping && (r.swipeLeft = t + o * s), !0 !== r.options.fade && !1 !== r.options.touchMove && (!0 === r.animating ? (r.swipeLeft = null, 
        !1) : void r.setCSS(r.swipeLeft))) : void 0);
    }, r.prototype.swipeStart = function(e) {
        var t, i = this;
        return i.interrupted = !0, 1 !== i.touchObject.fingerCount || i.slideCount <= i.options.slidesToShow ? !(i.touchObject = {}) : (void 0 !== e.originalEvent && void 0 !== e.originalEvent.touches && (t = e.originalEvent.touches[0]), 
        i.touchObject.startX = i.touchObject.curX = void 0 !== t ? t.pageX : e.clientX, 
        i.touchObject.startY = i.touchObject.curY = void 0 !== t ? t.pageY : e.clientY, 
        void (i.dragging = !0));
    }, r.prototype.unfilterSlides = r.prototype.slickUnfilter = function() {
        var e = this;
        null !== e.$slidesCache && (e.unload(), e.$slideTrack.children(this.options.slide).detach(), 
        e.$slidesCache.appendTo(e.$slideTrack), e.reinit());
    }, r.prototype.unload = function() {
        var e = this;
        d(".slick-cloned", e.$slider).remove(), e.$dots && e.$dots.remove(), e.$prevArrow && e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.remove(), 
        e.$nextArrow && e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.remove(), e.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden", "true").css("width", "");
    }, r.prototype.unslick = function(e) {
        this.$slider.trigger("unslick", [ this, e ]), this.destroy();
    }, r.prototype.updateArrows = function() {
        var e = this;
        Math.floor(e.options.slidesToShow / 2), !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && !e.options.infinite && (e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), 
        e.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), 0 === e.currentSlide ? (e.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true"), 
        e.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : e.currentSlide >= e.slideCount - e.options.slidesToShow && !1 === e.options.centerMode ? (e.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), 
        e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : e.currentSlide >= e.slideCount - 1 && !0 === e.options.centerMode && (e.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), 
        e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")));
    }, r.prototype.updateDots = function() {
        var e = this;
        null !== e.$dots && (e.$dots.find("li").removeClass("slick-active").attr("aria-hidden", "true"), 
        e.$dots.find("li").eq(Math.floor(e.currentSlide / e.options.slidesToScroll)).addClass("slick-active").attr("aria-hidden", "false"));
    }, r.prototype.visibility = function() {
        this.options.autoplay && (document[this.hidden] ? this.interrupted = !0 : this.interrupted = !1);
    }, d.fn.slick = function() {
        var e, t, i = this, o = arguments[0], s = Array.prototype.slice.call(arguments, 1), n = i.length;
        for (e = 0; e < n; e++) if ("object" == typeof o || void 0 === o ? i[e].slick = new r(i[e], o) : t = i[e].slick[o].apply(i[e].slick, s), 
        void 0 !== t) return t;
        return i;
    };
}), function(e) {
    "function" == typeof define && define.amd ? define([ "jquery" ], e) : e("object" == typeof exports ? require("jquery") : window.jQuery || window.Zepto);
}(function(c) {
    var p, o, u, s, f, t, l = "Close", d = "BeforeClose", h = "MarkupParse", g = "Open", m = ".mfp", v = "mfp-ready", i = "mfp-removing", r = "mfp-prevent-close", e = function() {}, a = !!window.jQuery, _ = c(window), y = function(e, t) {
        p.ev.on("mfp" + e + m, t);
    }, w = function(e, t, i, o) {
        var s = document.createElement("div");
        return s.className = "mfp-" + e, i && (s.innerHTML = i), o ? t && t.appendChild(s) : (s = c(s), 
        t && s.appendTo(t)), s;
    }, k = function(e, t) {
        p.ev.triggerHandler("mfp" + e, t), p.st.callbacks && (e = e.charAt(0).toLowerCase() + e.slice(1), 
        p.st.callbacks[e] && p.st.callbacks[e].apply(p, c.isArray(t) ? t : [ t ]));
    }, x = function(e) {
        return e === t && p.currTemplate.closeBtn || (p.currTemplate.closeBtn = c(p.st.closeMarkup.replace("%title%", p.st.tClose)), 
        t = e), p.currTemplate.closeBtn;
    }, n = function() {
        c.magnificPopup.instance || ((p = new e()).init(), c.magnificPopup.instance = p);
    };
    e.prototype = {
        constructor: e,
        init: function() {
            var e = navigator.appVersion;
            p.isLowIE = p.isIE8 = document.all && !document.addEventListener, p.isAndroid = /android/gi.test(e), 
            p.isIOS = /iphone|ipad|ipod/gi.test(e), p.supportsTransition = function() {
                var e = document.createElement("p").style, t = [ "ms", "O", "Moz", "Webkit" ];
                if (void 0 !== e.transition) return !0;
                for (;t.length; ) if (t.pop() + "Transition" in e) return !0;
                return !1;
            }(), p.probablyMobile = p.isAndroid || p.isIOS || /(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent), 
            u = c(document), p.popupsCache = {};
        },
        open: function(e) {
            var t;
            if (!1 === e.isObj) {
                p.items = e.items.toArray(), p.index = 0;
                var i, o = e.items;
                for (t = 0; t < o.length; t++) if ((i = o[t]).parsed && (i = i.el[0]), i === e.el[0]) {
                    p.index = t;
                    break;
                }
            } else p.items = c.isArray(e.items) ? e.items : [ e.items ], p.index = e.index || 0;
            if (!p.isOpen) {
                p.types = [], f = "", e.mainEl && e.mainEl.length ? p.ev = e.mainEl.eq(0) : p.ev = u, 
                e.key ? (p.popupsCache[e.key] || (p.popupsCache[e.key] = {}), p.currTemplate = p.popupsCache[e.key]) : p.currTemplate = {}, 
                p.st = c.extend(!0, {}, c.magnificPopup.defaults, e), p.fixedContentPos = "auto" === p.st.fixedContentPos ? !p.probablyMobile : p.st.fixedContentPos, 
                p.st.modal && (p.st.closeOnContentClick = !1, p.st.closeOnBgClick = !1, p.st.showCloseBtn = !1, 
                p.st.enableEscapeKey = !1), p.bgOverlay || (p.bgOverlay = w("bg").on("click" + m, function() {
                    p.close();
                }), p.wrap = w("wrap").attr("tabindex", -1).on("click" + m, function(e) {
                    p._checkIfClose(e.target) && p.close();
                }), p.container = w("container", p.wrap)), p.contentContainer = w("content"), p.st.preloader && (p.preloader = w("preloader", p.container, p.st.tLoading));
                var s = c.magnificPopup.modules;
                for (t = 0; t < s.length; t++) {
                    var n = s[t];
                    n = n.charAt(0).toUpperCase() + n.slice(1), p["init" + n].call(p);
                }
                k("BeforeOpen"), p.st.showCloseBtn && (p.st.closeBtnInside ? (y(h, function(e, t, i, o) {
                    i.close_replaceWith = x(o.type);
                }), f += " mfp-close-btn-in") : p.wrap.append(x())), p.st.alignTop && (f += " mfp-align-top"), 
                p.fixedContentPos ? p.wrap.css({
                    overflow: p.st.overflowY,
                    overflowX: "hidden",
                    overflowY: p.st.overflowY
                }) : p.wrap.css({
                    top: _.scrollTop(),
                    position: "absolute"
                }), (!1 === p.st.fixedBgPos || "auto" === p.st.fixedBgPos && !p.fixedContentPos) && p.bgOverlay.css({
                    height: u.height(),
                    position: "absolute"
                }), p.st.enableEscapeKey && u.on("keyup" + m, function(e) {
                    27 === e.keyCode && p.close();
                }), _.on("resize" + m, function() {
                    p.updateSize();
                }), p.st.closeOnContentClick || (f += " mfp-auto-cursor"), f && p.wrap.addClass(f);
                var r = p.wH = _.height(), a = {};
                if (p.fixedContentPos && p._hasScrollBar(r)) {
                    var l = p._getScrollbarSize();
                    l && (a.marginRight = l);
                }
                p.fixedContentPos && (p.isIE7 ? c("body, html").css("overflow", "hidden") : a.overflow = "hidden");
                var d = p.st.mainClass;
                return p.isIE7 && (d += " mfp-ie7"), d && p._addClassToMFP(d), p.updateItemHTML(), 
                k("BuildControls"), c("html").css(a), p.bgOverlay.add(p.wrap).prependTo(p.st.prependTo || c(document.body)), 
                p._lastFocusedEl = document.activeElement, setTimeout(function() {
                    p.content ? (p._addClassToMFP(v), p._setFocus()) : p.bgOverlay.addClass(v), u.on("focusin" + m, p._onFocusIn);
                }, 16), p.isOpen = !0, p.updateSize(r), k(g), e;
            }
            p.updateItemHTML();
        },
        close: function() {
            p.isOpen && (k(d), p.isOpen = !1, p.st.removalDelay && !p.isLowIE && p.supportsTransition ? (p._addClassToMFP(i), 
            setTimeout(function() {
                p._close();
            }, p.st.removalDelay)) : p._close());
        },
        _close: function() {
            k(l);
            var e = i + " " + v + " ";
            if (p.bgOverlay.detach(), p.wrap.detach(), p.container.empty(), p.st.mainClass && (e += p.st.mainClass + " "), 
            p._removeClassFromMFP(e), p.fixedContentPos) {
                var t = {
                    marginRight: ""
                };
                p.isIE7 ? c("body, html").css("overflow", "") : t.overflow = "", c("html").css(t);
            }
            u.off("keyup.mfp focusin" + m), p.ev.off(m), p.wrap.attr("class", "mfp-wrap").removeAttr("style"), 
            p.bgOverlay.attr("class", "mfp-bg"), p.container.attr("class", "mfp-container"), 
            !p.st.showCloseBtn || p.st.closeBtnInside && !0 !== p.currTemplate[p.currItem.type] || p.currTemplate.closeBtn && p.currTemplate.closeBtn.detach(), 
            p.st.autoFocusLast && p._lastFocusedEl && c(p._lastFocusedEl).focus(), p.currItem = null, 
            p.content = null, p.currTemplate = null, p.prevHeight = 0, k("AfterClose");
        },
        updateSize: function(e) {
            if (p.isIOS) {
                var t = document.documentElement.clientWidth / window.innerWidth, i = window.innerHeight * t;
                p.wrap.css("height", i), p.wH = i;
            } else p.wH = e || _.height();
            p.fixedContentPos || p.wrap.css("height", p.wH), k("Resize");
        },
        updateItemHTML: function() {
            var e = p.items[p.index];
            p.contentContainer.detach(), p.content && p.content.detach(), e.parsed || (e = p.parseEl(p.index));
            var t = e.type;
            if (k("BeforeChange", [ p.currItem ? p.currItem.type : "", t ]), p.currItem = e, 
            !p.currTemplate[t]) {
                var i = !!p.st[t] && p.st[t].markup;
                k("FirstMarkupParse", i), p.currTemplate[t] = !i || c(i);
            }
            s && s !== e.type && p.container.removeClass("mfp-" + s + "-holder");
            var o = p["get" + t.charAt(0).toUpperCase() + t.slice(1)](e, p.currTemplate[t]);
            p.appendContent(o, t), e.preloaded = !0, k("Change", e), s = e.type, p.container.prepend(p.contentContainer), 
            k("AfterChange");
        },
        appendContent: function(e, t) {
            (p.content = e) ? p.st.showCloseBtn && p.st.closeBtnInside && !0 === p.currTemplate[t] ? p.content.find(".mfp-close").length || p.content.append(x()) : p.content = e : p.content = "", 
            k("BeforeAppend"), p.container.addClass("mfp-" + t + "-holder"), p.contentContainer.append(p.content);
        },
        parseEl: function(e) {
            var t, i = p.items[e];
            if (i.tagName ? i = {
                el: c(i)
            } : (t = i.type, i = {
                data: i,
                src: i.src
            }), i.el) {
                for (var o = p.types, s = 0; s < o.length; s++) if (i.el.hasClass("mfp-" + o[s])) {
                    t = o[s];
                    break;
                }
                i.src = i.el.attr("data-mfp-src"), i.src || (i.src = i.el.attr("href"));
            }
            return i.type = t || p.st.type || "inline", i.index = e, i.parsed = !0, p.items[e] = i, 
            k("ElementParse", i), p.items[e];
        },
        addGroup: function(t, i) {
            var e = function(e) {
                e.mfpEl = this, p._openClick(e, t, i);
            };
            i || (i = {});
            var o = "click.magnificPopup";
            i.mainEl = t, i.items ? (i.isObj = !0, t.off(o).on(o, e)) : (i.isObj = !1, i.delegate ? t.off(o).on(o, i.delegate, e) : (i.items = t).off(o).on(o, e));
        },
        _openClick: function(e, t, i) {
            if ((void 0 !== i.midClick ? i.midClick : c.magnificPopup.defaults.midClick) || !(2 === e.which || e.ctrlKey || e.metaKey || e.altKey || e.shiftKey)) {
                var o = void 0 !== i.disableOn ? i.disableOn : c.magnificPopup.defaults.disableOn;
                if (o) if (c.isFunction(o)) {
                    if (!o.call(p)) return !0;
                } else if (_.width() < o) return !0;
                e.type && (e.preventDefault(), p.isOpen && e.stopPropagation()), i.el = c(e.mfpEl), 
                i.delegate && (i.items = t.find(i.delegate)), p.open(i);
            }
        },
        updateStatus: function(e, t) {
            if (p.preloader) {
                o !== e && p.container.removeClass("mfp-s-" + o), t || "loading" !== e || (t = p.st.tLoading);
                var i = {
                    status: e,
                    text: t
                };
                k("UpdateStatus", i), e = i.status, t = i.text, p.preloader.html(t), p.preloader.find("a").on("click", function(e) {
                    e.stopImmediatePropagation();
                }), p.container.addClass("mfp-s-" + e), o = e;
            }
        },
        _checkIfClose: function(e) {
            if (!c(e).hasClass(r)) {
                var t = p.st.closeOnContentClick, i = p.st.closeOnBgClick;
                if (t && i) return !0;
                if (!p.content || c(e).hasClass("mfp-close") || p.preloader && e === p.preloader[0]) return !0;
                if (e === p.content[0] || c.contains(p.content[0], e)) {
                    if (t) return !0;
                } else if (i && c.contains(document, e)) return !0;
                return !1;
            }
        },
        _addClassToMFP: function(e) {
            p.bgOverlay.addClass(e), p.wrap.addClass(e);
        },
        _removeClassFromMFP: function(e) {
            this.bgOverlay.removeClass(e), p.wrap.removeClass(e);
        },
        _hasScrollBar: function(e) {
            return (p.isIE7 ? u.height() : document.body.scrollHeight) > (e || _.height());
        },
        _setFocus: function() {
            (p.st.focus ? p.content.find(p.st.focus).eq(0) : p.wrap).focus();
        },
        _onFocusIn: function(e) {
            return e.target === p.wrap[0] || c.contains(p.wrap[0], e.target) ? void 0 : (p._setFocus(), 
            !1);
        },
        _parseMarkup: function(s, e, t) {
            var n;
            t.data && (e = c.extend(t.data, e)), k(h, [ s, e, t ]), c.each(e, function(e, t) {
                if (void 0 === t || !1 === t) return !0;
                if (1 < (n = e.split("_")).length) {
                    var i = s.find(m + "-" + n[0]);
                    if (0 < i.length) {
                        var o = n[1];
                        "replaceWith" === o ? i[0] !== t[0] && i.replaceWith(t) : "img" === o ? i.is("img") ? i.attr("src", t) : i.replaceWith(c("<img>").attr("src", t).attr("class", i.attr("class"))) : i.attr(n[1], t);
                    }
                } else s.find(m + "-" + e).html(t);
            });
        },
        _getScrollbarSize: function() {
            if (void 0 === p.scrollbarSize) {
                var e = document.createElement("div");
                e.style.cssText = "width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;", 
                document.body.appendChild(e), p.scrollbarSize = e.offsetWidth - e.clientWidth, document.body.removeChild(e);
            }
            return p.scrollbarSize;
        }
    }, c.magnificPopup = {
        instance: null,
        proto: e.prototype,
        modules: [],
        open: function(e, t) {
            return n(), (e = e ? c.extend(!0, {}, e) : {}).isObj = !0, e.index = t || 0, this.instance.open(e);
        },
        close: function() {
            return c.magnificPopup.instance && c.magnificPopup.instance.close();
        },
        registerModule: function(e, t) {
            t.options && (c.magnificPopup.defaults[e] = t.options), c.extend(this.proto, t.proto), 
            this.modules.push(e);
        },
        defaults: {
            disableOn: 0,
            key: null,
            midClick: !1,
            mainClass: "",
            preloader: !0,
            focus: "",
            closeOnContentClick: !1,
            closeOnBgClick: !0,
            closeBtnInside: !0,
            showCloseBtn: !0,
            enableEscapeKey: !0,
            modal: !1,
            alignTop: !1,
            removalDelay: 0,
            prependTo: null,
            fixedContentPos: "auto",
            fixedBgPos: "auto",
            overflowY: "auto",
            closeMarkup: '<button title="%title%" type="button" class="mfp-close">&#215;</button>',
            tClose: "Close (Esc)",
            tLoading: "Loading...",
            autoFocusLast: !0
        }
    }, c.fn.magnificPopup = function(e) {
        n();
        var t = c(this);
        if ("string" == typeof e) if ("open" === e) {
            var i, o = a ? t.data("magnificPopup") : t[0].magnificPopup, s = parseInt(arguments[1], 10) || 0;
            o.items ? i = o.items[s] : (i = t, o.delegate && (i = i.find(o.delegate)), i = i.eq(s)), 
            p._openClick({
                mfpEl: i
            }, t, o);
        } else p.isOpen && p[e].apply(p, Array.prototype.slice.call(arguments, 1)); else e = c.extend(!0, {}, e), 
        a ? t.data("magnificPopup", e) : t[0].magnificPopup = e, p.addGroup(t, e);
        return t;
    };
    var b, S, C, T = "inline", $ = function() {
        C && (S.after(C.addClass(b)).detach(), C = null);
    };
    c.magnificPopup.registerModule(T, {
        options: {
            hiddenClass: "hide",
            markup: "",
            tNotFound: "Content not found"
        },
        proto: {
            initInline: function() {
                p.types.push(T), y(l + "." + T, function() {
                    $();
                });
            },
            getInline: function(e, t) {
                if ($(), e.src) {
                    var i = p.st.inline, o = c(e.src);
                    if (o.length) {
                        var s = o[0].parentNode;
                        s && s.tagName && (S || (b = i.hiddenClass, S = w(b), b = "mfp-" + b), C = o.after(S).detach().removeClass(b)), 
                        p.updateStatus("ready");
                    } else p.updateStatus("error", i.tNotFound), o = c("<div>");
                    return e.inlineElement = o;
                }
                return p.updateStatus("ready"), p._parseMarkup(t, {}, e), t;
            }
        }
    });
    var A, M = "ajax", I = function() {
        A && c(document.body).removeClass(A);
    }, O = function() {
        I(), p.req && p.req.abort();
    };
    c.magnificPopup.registerModule(M, {
        options: {
            settings: null,
            cursor: "mfp-ajax-cur",
            tError: '<a href="%url%">The content</a> could not be loaded.'
        },
        proto: {
            initAjax: function() {
                p.types.push(M), A = p.st.ajax.cursor, y(l + "." + M, O), y("BeforeChange." + M, O);
            },
            getAjax: function(s) {
                A && c(document.body).addClass(A), p.updateStatus("loading");
                var e = c.extend({
                    url: s.src,
                    success: function(e, t, i) {
                        var o = {
                            data: e,
                            xhr: i
                        };
                        k("ParseAjax", o), p.appendContent(c(o.data), M), s.finished = !0, I(), p._setFocus(), 
                        setTimeout(function() {
                            p.wrap.addClass(v);
                        }, 16), p.updateStatus("ready"), k("AjaxContentAdded");
                    },
                    error: function() {
                        I(), s.finished = s.loadError = !0, p.updateStatus("error", p.st.ajax.tError.replace("%url%", s.src));
                    }
                }, p.st.ajax.settings);
                return p.req = c.ajax(e), "";
            }
        }
    });
    var P;
    c.magnificPopup.registerModule("image", {
        options: {
            markup: '<div class="mfp-figure"><div class="mfp-close"></div><figure><div class="mfp-img"></div><figcaption><div class="mfp-bottom-bar"><div class="mfp-title"></div><div class="mfp-counter"></div></div></figcaption></figure></div>',
            cursor: "mfp-zoom-out-cur",
            titleSrc: "title",
            verticalFit: !0,
            tError: '<a href="%url%">The image</a> could not be loaded.'
        },
        proto: {
            initImage: function() {
                var e = p.st.image, t = ".image";
                p.types.push("image"), y(g + t, function() {
                    "image" === p.currItem.type && e.cursor && c(document.body).addClass(e.cursor);
                }), y(l + t, function() {
                    e.cursor && c(document.body).removeClass(e.cursor), _.off("resize" + m);
                }), y("Resize" + t, p.resizeImage), p.isLowIE && y("AfterChange", p.resizeImage);
            },
            resizeImage: function() {
                var e = p.currItem;
                if (e && e.img && p.st.image.verticalFit) {
                    var t = 0;
                    p.isLowIE && (t = parseInt(e.img.css("padding-top"), 10) + parseInt(e.img.css("padding-bottom"), 10)), 
                    e.img.css("max-height", p.wH - t);
                }
            },
            _onImageHasSize: function(e) {
                e.img && (e.hasSize = !0, P && clearInterval(P), e.isCheckingImgSize = !1, k("ImageHasSize", e), 
                e.imgHidden && (p.content && p.content.removeClass("mfp-loading"), e.imgHidden = !1));
            },
            findImageSize: function(t) {
                var i = 0, o = t.img[0], s = function(e) {
                    P && clearInterval(P), P = setInterval(function() {
                        return 0 < o.naturalWidth ? void p._onImageHasSize(t) : (200 < i && clearInterval(P), 
                        void (3 === ++i ? s(10) : 40 === i ? s(50) : 100 === i && s(500)));
                    }, e);
                };
                s(1);
            },
            getImage: function(e, t) {
                var i = 0, o = function() {
                    e && (e.img[0].complete ? (e.img.off(".mfploader"), e === p.currItem && (p._onImageHasSize(e), 
                    p.updateStatus("ready")), e.hasSize = !0, e.loaded = !0, k("ImageLoadComplete")) : ++i < 200 ? setTimeout(o, 100) : s());
                }, s = function() {
                    e && (e.img.off(".mfploader"), e === p.currItem && (p._onImageHasSize(e), p.updateStatus("error", n.tError.replace("%url%", e.src))), 
                    e.hasSize = !0, e.loaded = !0, e.loadError = !0);
                }, n = p.st.image, r = t.find(".mfp-img");
                if (r.length) {
                    var a = document.createElement("img");
                    a.className = "mfp-img", e.el && e.el.find("img").length && (a.alt = e.el.find("img").attr("alt")), 
                    e.img = c(a).on("load.mfploader", o).on("error.mfploader", s), a.src = e.src, r.is("img") && (e.img = e.img.clone()), 
                    0 < (a = e.img[0]).naturalWidth ? e.hasSize = !0 : a.width || (e.hasSize = !1);
                }
                return p._parseMarkup(t, {
                    title: function(e) {
                        if (e.data && void 0 !== e.data.title) return e.data.title;
                        var t = p.st.image.titleSrc;
                        if (t) {
                            if (c.isFunction(t)) return t.call(p, e);
                            if (e.el) return e.el.attr(t) || "";
                        }
                        return "";
                    }(e),
                    img_replaceWith: e.img
                }, e), p.resizeImage(), e.hasSize ? (P && clearInterval(P), e.loadError ? (t.addClass("mfp-loading"), 
                p.updateStatus("error", n.tError.replace("%url%", e.src))) : (t.removeClass("mfp-loading"), 
                p.updateStatus("ready"))) : (p.updateStatus("loading"), e.loading = !0, e.hasSize || (e.imgHidden = !0, 
                t.addClass("mfp-loading"), p.findImageSize(e))), t;
            }
        }
    });
    var H;
    c.magnificPopup.registerModule("zoom", {
        options: {
            enabled: !1,
            easing: "ease-in-out",
            duration: 300,
            opener: function(e) {
                return e.is("img") ? e : e.find("img");
            }
        },
        proto: {
            initZoom: function() {
                var e, n = p.st.zoom, t = ".zoom";
                if (n.enabled && p.supportsTransition) {
                    var i, o, s = n.duration, r = function(e) {
                        var t = e.clone().removeAttr("style").removeAttr("class").addClass("mfp-animated-image"), i = "all " + n.duration / 1e3 + "s " + n.easing, o = {
                            position: "fixed",
                            zIndex: 9999,
                            left: 0,
                            top: 0,
                            "-webkit-backface-visibility": "hidden"
                        }, s = "transition";
                        return o["-webkit-" + s] = o["-moz-" + s] = o["-o-" + s] = o[s] = i, t.css(o), t;
                    }, a = function() {
                        p.content.css("visibility", "visible");
                    };
                    y("BuildControls" + t, function() {
                        if (p._allowZoom()) {
                            if (clearTimeout(i), p.content.css("visibility", "hidden"), !(e = p._getItemToZoom())) return void a();
                            (o = r(e)).css(p._getOffset()), p.wrap.append(o), i = setTimeout(function() {
                                o.css(p._getOffset(!0)), i = setTimeout(function() {
                                    a(), setTimeout(function() {
                                        o.remove(), e = o = null, k("ZoomAnimationEnded");
                                    }, 16);
                                }, s);
                            }, 16);
                        }
                    }), y(d + t, function() {
                        if (p._allowZoom()) {
                            if (clearTimeout(i), p.st.removalDelay = s, !e) {
                                if (!(e = p._getItemToZoom())) return;
                                o = r(e);
                            }
                            o.css(p._getOffset(!0)), p.wrap.append(o), p.content.css("visibility", "hidden"), 
                            setTimeout(function() {
                                o.css(p._getOffset());
                            }, 16);
                        }
                    }), y(l + t, function() {
                        p._allowZoom() && (a(), o && o.remove(), e = null);
                    });
                }
            },
            _allowZoom: function() {
                return "image" === p.currItem.type;
            },
            _getItemToZoom: function() {
                return !!p.currItem.hasSize && p.currItem.img;
            },
            _getOffset: function(e) {
                var t, i = (t = e ? p.currItem.img : p.st.zoom.opener(p.currItem.el || p.currItem)).offset(), o = parseInt(t.css("padding-top"), 10), s = parseInt(t.css("padding-bottom"), 10);
                i.top -= c(window).scrollTop() - o;
                var n = {
                    width: t.width(),
                    height: (a ? t.innerHeight() : t[0].offsetHeight) - s - o
                };
                return void 0 === H && (H = void 0 !== document.createElement("p").style.MozTransform), 
                H ? n["-moz-transform"] = n.transform = "translate(" + i.left + "px," + i.top + "px)" : (n.left = i.left, 
                n.top = i.top), n;
            }
        }
    });
    var z = "iframe", E = function(e) {
        if (p.currTemplate[z]) {
            var t = p.currTemplate[z].find("iframe");
            t.length && (e || (t[0].src = "//about:blank"), p.isIE8 && t.css("display", e ? "block" : "none"));
        }
    };
    c.magnificPopup.registerModule(z, {
        options: {
            markup: '<div class="mfp-iframe-scaler"><div class="mfp-close"></div><iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe></div>',
            srcAction: "iframe_src",
            patterns: {
                youtube: {
                    index: "youtube.com",
                    id: "v=",
                    src: "//www.youtube.com/embed/%id%?autoplay=1"
                },
                vimeo: {
                    index: "vimeo.com/",
                    id: "/",
                    src: "//player.vimeo.com/video/%id%?autoplay=1"
                },
                gmaps: {
                    index: "//maps.google.",
                    src: "%id%&output=embed"
                }
            }
        },
        proto: {
            initIframe: function() {
                p.types.push(z), y("BeforeChange", function(e, t, i) {
                    t !== i && (t === z ? E() : i === z && E(!0));
                }), y(l + "." + z, function() {
                    E();
                });
            },
            getIframe: function(e, t) {
                var i = e.src, o = p.st.iframe;
                c.each(o.patterns, function() {
                    return -1 < i.indexOf(this.index) ? (this.id && (i = "string" == typeof this.id ? i.substr(i.lastIndexOf(this.id) + this.id.length, i.length) : this.id.call(this, i)), 
                    i = this.src.replace("%id%", i), !1) : void 0;
                });
                var s = {};
                return o.srcAction && (s[o.srcAction] = i), p._parseMarkup(t, s, e), p.updateStatus("ready"), 
                t;
            }
        }
    });
    var D = function(e) {
        var t = p.items.length;
        return t - 1 < e ? e - t : e < 0 ? t + e : e;
    }, L = function(e, t, i) {
        return e.replace(/%curr%/gi, t + 1).replace(/%total%/gi, i);
    };
    c.magnificPopup.registerModule("gallery", {
        options: {
            enabled: !1,
            arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
            preload: [ 0, 2 ],
            navigateByImgClick: !0,
            arrows: !0,
            tPrev: "Previous (Left arrow key)",
            tNext: "Next (Right arrow key)",
            tCounter: "%curr% of %total%"
        },
        proto: {
            initGallery: function() {
                var n = p.st.gallery, e = ".mfp-gallery";
                return p.direction = !0, !(!n || !n.enabled) && (f += " mfp-gallery", y(g + e, function() {
                    n.navigateByImgClick && p.wrap.on("click" + e, ".mfp-img", function() {
                        return 1 < p.items.length ? (p.next(), !1) : void 0;
                    }), u.on("keydown" + e, function(e) {
                        37 === e.keyCode ? p.prev() : 39 === e.keyCode && p.next();
                    });
                }), y("UpdateStatus" + e, function(e, t) {
                    t.text && (t.text = L(t.text, p.currItem.index, p.items.length));
                }), y(h + e, function(e, t, i, o) {
                    var s = p.items.length;
                    i.counter = 1 < s ? L(n.tCounter, o.index, s) : "";
                }), y("BuildControls" + e, function() {
                    if (1 < p.items.length && n.arrows && !p.arrowLeft) {
                        var e = n.arrowMarkup, t = p.arrowLeft = c(e.replace(/%title%/gi, n.tPrev).replace(/%dir%/gi, "left")).addClass(r), i = p.arrowRight = c(e.replace(/%title%/gi, n.tNext).replace(/%dir%/gi, "right")).addClass(r);
                        t.click(function() {
                            p.prev();
                        }), i.click(function() {
                            p.next();
                        }), p.container.append(t.add(i));
                    }
                }), y("Change" + e, function() {
                    p._preloadTimeout && clearTimeout(p._preloadTimeout), p._preloadTimeout = setTimeout(function() {
                        p.preloadNearbyImages(), p._preloadTimeout = null;
                    }, 16);
                }), void y(l + e, function() {
                    u.off(e), p.wrap.off("click" + e), p.arrowRight = p.arrowLeft = null;
                }));
            },
            next: function() {
                p.direction = !0, p.index = D(p.index + 1), p.updateItemHTML();
            },
            prev: function() {
                p.direction = !1, p.index = D(p.index - 1), p.updateItemHTML();
            },
            goTo: function(e) {
                p.direction = e >= p.index, p.index = e, p.updateItemHTML();
            },
            preloadNearbyImages: function() {
                var e, t = p.st.gallery.preload, i = Math.min(t[0], p.items.length), o = Math.min(t[1], p.items.length);
                for (e = 1; e <= (p.direction ? o : i); e++) p._preloadItem(p.index + e);
                for (e = 1; e <= (p.direction ? i : o); e++) p._preloadItem(p.index - e);
            },
            _preloadItem: function(e) {
                if (e = D(e), !p.items[e].preloaded) {
                    var t = p.items[e];
                    t.parsed || (t = p.parseEl(e)), k("LazyLoad", t), "image" === t.type && (t.img = c('<img class="mfp-img" />').on("load.mfploader", function() {
                        t.hasSize = !0;
                    }).on("error.mfploader", function() {
                        t.hasSize = !0, t.loadError = !0, k("LazyLoadError", t);
                    }).attr("src", t.src)), t.preloaded = !0;
                }
            }
        }
    });
    var F = "retina";
    c.magnificPopup.registerModule(F, {
        options: {
            replaceSrc: function(e) {
                return e.src.replace(/\.\w+$/, function(e) {
                    return "@2x" + e;
                });
            },
            ratio: 1
        },
        proto: {
            initRetina: function() {
                if (1 < window.devicePixelRatio) {
                    var i = p.st.retina, o = i.ratio;
                    1 < (o = isNaN(o) ? o() : o) && (y("ImageHasSize." + F, function(e, t) {
                        t.img.css({
                            "max-width": t.img[0].naturalWidth / o,
                            width: "100%"
                        });
                    }), y("ElementParse." + F, function(e, t) {
                        t.src = i.replaceSrc(t, o);
                    }));
                }
            }
        }
    }), n();
}), function(c, p) {
    "use strict";
    c.MixItUp = function() {
        this._execAction("_constructor", 0), c.extend(this, {
            selectors: {
                target: ".mix",
                filter: ".filter",
                sort: ".sort"
            },
            animation: {
                enable: !0,
                effects: "fade scale",
                duration: 1600,
                easing: "ease",
                perspectiveDistance: "3000",
                perspectiveOrigin: "50% 50%",
                queue: !0,
                queueLimit: 1,
                animateChangeLayout: !1,
                animateResizeContainer: !0,
                animateResizeTargets: !1,
                staggerSequence: !1,
                reverseOut: !1
            },
            callbacks: {
                onMixLoad: !1,
                onMixStart: !1,
                onMixBusy: !1,
                onMixEnd: !1,
                onMixFail: !1,
                _user: !1
            },
            controls: {
                enable: !0,
                live: !1,
                toggleFilterButtons: !1,
                toggleLogic: "or",
                activeClass: "active"
            },
            layout: {
                display: "inline-block",
                containerClass: "",
                containerClassFail: "fail"
            },
            load: {
                filter: "all",
                sort: !1
            },
            _$body: null,
            _$container: null,
            _$targets: null,
            _$parent: null,
            _$sortButtons: null,
            _$filterButtons: null,
            _suckMode: !1,
            _mixing: !1,
            _sorting: !1,
            _clicking: !1,
            _loading: !0,
            _changingLayout: !1,
            _changingClass: !1,
            _changingDisplay: !1,
            _origOrder: [],
            _startOrder: [],
            _newOrder: [],
            _activeFilter: null,
            _toggleArray: [],
            _toggleString: "",
            _activeSort: "default:asc",
            _newSort: null,
            _startHeight: null,
            _newHeight: null,
            _incPadding: !0,
            _newDisplay: null,
            _newClass: null,
            _targetsBound: 0,
            _targetsDone: 0,
            _queue: [],
            _$show: c(),
            _$hide: c()
        }), this._execAction("_constructor", 1);
    }, c.MixItUp.prototype = {
        constructor: c.MixItUp,
        _instances: {},
        _handled: {
            _filter: {},
            _sort: {}
        },
        _bound: {
            _filter: {},
            _sort: {}
        },
        _actions: {},
        _filters: {},
        extend: function(e) {
            for (var t in e) c.MixItUp.prototype[t] = e[t];
        },
        addAction: function(e, t, i, o) {
            c.MixItUp.prototype._addHook("_actions", e, t, i, o);
        },
        addFilter: function(e, t, i, o) {
            c.MixItUp.prototype._addHook("_filters", e, t, i, o);
        },
        _addHook: function(e, t, i, o, s) {
            var n = c.MixItUp.prototype[e], r = {};
            s = 1 === s || "post" === s ? "post" : "pre", r[t] = {}, r[t][s] = {}, r[t][s][i] = o, 
            c.extend(!0, n, r);
        },
        _init: function(e, t) {
            var i = this;
            if (i._execAction("_init", 0, arguments), t && c.extend(!0, i, t), i._$body = c("body"), 
            i._domNode = e, i._$container = c(e), i._$container.addClass(i.layout.containerClass), 
            i._id = e.id, i._platformDetect(), i._brake = i._getPrefixedCSS("transition", "none"), 
            i._refresh(!0), i._$parent = i._$targets.parent().length ? i._$targets.parent() : i._$container, 
            i.load.sort && (i._newSort = i._parseSort(i.load.sort), i._newSortString = i.load.sort, 
            i._activeSort = i.load.sort, i._sort(), i._printSort()), i._activeFilter = "all" === i.load.filter ? i.selectors.target : "none" === i.load.filter ? "" : i.load.filter, 
            i.controls.enable && i._bindHandlers(), i.controls.toggleFilterButtons) {
                i._buildToggleArray();
                for (var o = 0; o < i._toggleArray.length; o++) i._updateControls({
                    filter: i._toggleArray[o],
                    sort: i._activeSort
                }, !0);
            } else i.controls.enable && i._updateControls({
                filter: i._activeFilter,
                sort: i._activeSort
            });
            i._filter(), i._init = !0, i._$container.data("mixItUp", i), i._execAction("_init", 1, arguments), 
            i._buildState(), i._$targets.css(i._brake), i._goMix(i.animation.enable);
        },
        _platformDetect: function() {
            var e = this, i = [ "Webkit", "Moz", "O", "ms" ], t = [ "webkit", "moz" ], o = window.navigator.appVersion.match(/Chrome\/(\d+)\./) || !1, s = "undefined" != typeof InstallTrigger, n = function(e) {
                for (var t = 0; t < i.length; t++) if (i[t] + "Transition" in e.style) return {
                    prefix: "-" + i[t].toLowerCase() + "-",
                    vendor: i[t]
                };
                return "transition" in e.style && "";
            }(e._domNode);
            e._execAction("_platformDetect", 0), e._chrome = !!o && parseInt(o[1], 10), e._ff = !!s && parseInt(window.navigator.userAgent.match(/rv:([^)]+)\)/)[1]), 
            e._prefix = n.prefix, e._vendor = n.vendor, e._suckMode = !window.atob || !e._prefix, 
            e._suckMode && (e.animation.enable = !1), e._ff && e._ff <= 4 && (e.animation.enable = !1);
            for (var r = 0; r < t.length && !window.requestAnimationFrame; r++) window.requestAnimationFrame = window[t[r] + "RequestAnimationFrame"];
            "function" != typeof Object.getPrototypeOf && ("object" == typeof "test".__proto__ ? Object.getPrototypeOf = function(e) {
                return e.__proto__;
            } : Object.getPrototypeOf = function(e) {
                return e.constructor.prototype;
            }), e._domNode.nextElementSibling === p && Object.defineProperty(Element.prototype, "nextElementSibling", {
                get: function() {
                    for (var e = this.nextSibling; e; ) {
                        if (1 === e.nodeType) return e;
                        e = e.nextSibling;
                    }
                    return null;
                }
            }), e._execAction("_platformDetect", 1);
        },
        _refresh: function(e, t) {
            var i = this;
            i._execAction("_refresh", 0, arguments), i._$targets = i._$container.find(i.selectors.target);
            for (var o = 0; o < i._$targets.length; o++) {
                if ((d = i._$targets[o]).dataset === p || t) {
                    d.dataset = {};
                    for (var s = 0; s < d.attributes.length; s++) {
                        var n = d.attributes[s], r = n.name, a = n.value;
                        if (-1 < r.indexOf("data-")) {
                            var l = i._helpers._camelCase(r.substring(5, r.length));
                            d.dataset[l] = a;
                        }
                    }
                }
                d.mixParent === p && (d.mixParent = i._id);
            }
            if (i._$targets.length && e || !i._origOrder.length && i._$targets.length) {
                i._origOrder = [];
                for (o = 0; o < i._$targets.length; o++) {
                    var d = i._$targets[o];
                    i._origOrder.push(d);
                }
            }
            i._execAction("_refresh", 1, arguments);
        },
        _bindHandlers: function() {
            var e = this, t = c.MixItUp.prototype._bound._filter, i = c.MixItUp.prototype._bound._sort;
            e._execAction("_bindHandlers", 0), e.controls.live ? e._$body.on("click.mixItUp." + e._id, e.selectors.sort, function() {
                e._processClick(c(this), "sort");
            }).on("click.mixItUp." + e._id, e.selectors.filter, function() {
                e._processClick(c(this), "filter");
            }) : (e._$sortButtons = c(e.selectors.sort), e._$filterButtons = c(e.selectors.filter), 
            e._$sortButtons.on("click.mixItUp." + e._id, function() {
                e._processClick(c(this), "sort");
            }), e._$filterButtons.on("click.mixItUp." + e._id, function() {
                e._processClick(c(this), "filter");
            })), t[e.selectors.filter] = t[e.selectors.filter] === p ? 1 : t[e.selectors.filter] + 1, 
            i[e.selectors.sort] = i[e.selectors.sort] === p ? 1 : i[e.selectors.sort] + 1, e._execAction("_bindHandlers", 1);
        },
        _processClick: function(e, t) {
            var s = this, i = function(e, t, i) {
                var o = c.MixItUp.prototype;
                o._handled["_" + t][s.selectors[t]] = o._handled["_" + t][s.selectors[t]] === p ? 1 : o._handled["_" + t][s.selectors[t]] + 1, 
                o._handled["_" + t][s.selectors[t]] === o._bound["_" + t][s.selectors[t]] && (e[(i ? "remove" : "add") + "Class"](s.controls.activeClass), 
                delete o._handled["_" + t][s.selectors[t]]);
            };
            if (s._execAction("_processClick", 0, arguments), !s._mixing || s.animation.queue && s._queue.length < s.animation.queueLimit) {
                if (s._clicking = !0, "sort" === t) {
                    var o = e.attr("data-sort");
                    (!e.hasClass(s.controls.activeClass) || -1 < o.indexOf("random")) && (c(s.selectors.sort).removeClass(s.controls.activeClass), 
                    i(e, t), s.sort(o));
                }
                if ("filter" === t) {
                    var n, r = e.attr("data-filter"), a = "or" === s.controls.toggleLogic ? "," : "";
                    s.controls.toggleFilterButtons ? (s._buildToggleArray(), e.hasClass(s.controls.activeClass) ? (i(e, t, !0), 
                    n = s._toggleArray.indexOf(r), s._toggleArray.splice(n, 1)) : (i(e, t), s._toggleArray.push(r)), 
                    s._toggleArray = c.grep(s._toggleArray, function(e) {
                        return e;
                    }), s._toggleString = s._toggleArray.join(a), s.filter(s._toggleString)) : e.hasClass(s.controls.activeClass) || (c(s.selectors.filter).removeClass(s.controls.activeClass), 
                    i(e, t), s.filter(r));
                }
                s._execAction("_processClick", 1, arguments);
            } else "function" == typeof s.callbacks.onMixBusy && s.callbacks.onMixBusy.call(s._domNode, s._state, s), 
            s._execAction("_processClickBusy", 1, arguments);
        },
        _buildToggleArray: function() {
            var e = this, t = e._activeFilter.replace(/\s/g, "");
            if (e._execAction("_buildToggleArray", 0, arguments), "or" === e.controls.toggleLogic) e._toggleArray = t.split(","); else {
                e._toggleArray = t.split("."), !e._toggleArray[0] && e._toggleArray.shift();
                for (var i, o = 0; i = e._toggleArray[o]; o++) e._toggleArray[o] = "." + i;
            }
            e._execAction("_buildToggleArray", 1, arguments);
        },
        _updateControls: function(e, i) {
            var o = this, s = {
                filter: e.filter,
                sort: e.sort
            }, t = function(e, t) {
                try {
                    i && "filter" === n && "none" !== s.filter && "" !== s.filter ? e.filter(t).addClass(o.controls.activeClass) : e.removeClass(o.controls.activeClass).filter(t).addClass(o.controls.activeClass);
                } catch (e) {}
            }, n = "filter", r = null;
            o._execAction("_updateControls", 0, arguments), e.filter === p && (s.filter = o._activeFilter), 
            e.sort === p && (s.sort = o._activeSort), s.filter === o.selectors.target && (s.filter = "all");
            for (var a = 0; a < 2; a++) (r = o.controls.live ? c(o.selectors[n]) : o["_$" + n + "Buttons"]) && t(r, "[data-" + n + '="' + s[n] + '"]'), 
            n = "sort";
            o._execAction("_updateControls", 1, arguments);
        },
        _filter: function() {
            var e = this;
            e._execAction("_filter", 0);
            for (var t = 0; t < e._$targets.length; t++) {
                var i = c(e._$targets[t]);
                i.is(e._activeFilter) ? e._$show = e._$show.add(i) : e._$hide = e._$hide.add(i);
            }
            e._execAction("_filter", 1);
        },
        _sort: function() {
            var i = this;
            i._execAction("_sort", 0), i._startOrder = [];
            for (var e = 0; e < i._$targets.length; e++) {
                var t = i._$targets[e];
                i._startOrder.push(t);
            }
            switch (i._newSort[0].sortBy) {
              case "default":
                i._newOrder = i._origOrder;
                break;

              case "random":
                i._newOrder = function(e) {
                    for (var t = e.slice(), i = t.length, o = i; o--; ) {
                        var s = parseInt(Math.random() * i), n = t[o];
                        t[o] = t[s], t[s] = n;
                    }
                    return t;
                }(i._startOrder);
                break;

              case "custom":
                i._newOrder = i._newSort[0].order;
                break;

              default:
                i._newOrder = i._startOrder.concat().sort(function(e, t) {
                    return i._compare(e, t);
                });
            }
            i._execAction("_sort", 1);
        },
        _compare: function(e, t, i) {
            i = i || 0;
            var o = this, s = o._newSort[i].order, n = function(e) {
                return e.dataset[o._newSort[i].sortBy] || 0;
            }, r = isNaN(1 * n(e)) ? n(e).toLowerCase() : 1 * n(e), a = isNaN(1 * n(t)) ? n(t).toLowerCase() : 1 * n(t);
            return r < a ? "asc" === s ? -1 : 1 : a < r ? "asc" === s ? 1 : -1 : r === a && o._newSort.length > i + 1 ? o._compare(e, t, i + 1) : 0;
        },
        _printSort: function(e) {
            var t = this, i = e ? t._startOrder : t._newOrder, o = t._$parent[0].querySelectorAll(t.selectors.target), s = o.length ? o[o.length - 1].nextElementSibling : null, n = document.createDocumentFragment();
            t._execAction("_printSort", 0, arguments);
            for (var r = 0; r < o.length; r++) {
                var a = o[r], l = a.nextSibling;
                "absolute" !== a.style.position && (l && "#text" === l.nodeName && t._$parent[0].removeChild(l), 
                t._$parent[0].removeChild(a));
            }
            for (r = 0; r < i.length; r++) {
                var d = i[r];
                if ("default" !== t._newSort[0].sortBy || "desc" !== t._newSort[0].order || e) n.appendChild(d), 
                n.appendChild(document.createTextNode(" ")); else {
                    var c = n.firstChild;
                    n.insertBefore(d, c), n.insertBefore(document.createTextNode(" "), d);
                }
            }
            s ? t._$parent[0].insertBefore(n, s) : t._$parent[0].appendChild(n), t._execAction("_printSort", 1, arguments);
        },
        _parseSort: function(e) {
            for (var t = "string" == typeof e ? e.split(" ") : [ e ], i = [], o = 0; o < t.length; o++) {
                var s = "string" == typeof e ? t[o].split(":") : [ "custom", t[o] ], n = {
                    sortBy: this._helpers._camelCase(s[0]),
                    order: s[1] || "asc"
                };
                if (i.push(n), "default" === n.sortBy || "random" === n.sortBy) break;
            }
            return this._execFilter("_parseSort", i, arguments);
        },
        _parseEffects: function() {
            var n = this, l = {
                opacity: "",
                transformIn: "",
                transformOut: "",
                filter: ""
            }, d = function(e, t, i) {
                if (-1 < n.animation.effects.indexOf(e)) {
                    if (t) {
                        var o = n.animation.effects.indexOf(e + "(");
                        if (-1 < o) {
                            var s = n.animation.effects.substring(o);
                            return {
                                val: /\(([^)]+)\)/.exec(s)[1]
                            };
                        }
                    }
                    return !0;
                }
                return !1;
            }, e = function(e, t) {
                for (var i = [ [ "scale", ".01" ], [ "translateX", "20px" ], [ "translateY", "20px" ], [ "translateZ", "20px" ], [ "rotateX", "90deg" ], [ "rotateY", "90deg" ], [ "rotateZ", "180deg" ] ], o = 0; o < i.length; o++) {
                    var s = i[o][0], n = i[o][1], r = t && "scale" !== s;
                    l[e] += d(s) ? s + "(" + (a = d(s, !0).val || n, r ? "-" === a.charAt(0) ? a.substr(1, a.length) : "-" + a : a) + ") " : "";
                }
                var a;
            };
            return l.opacity = d("fade") ? d("fade", !0).val || "0" : "1", e("transformIn"), 
            n.animation.reverseOut ? e("transformOut", !0) : l.transformOut = l.transformIn, 
            l.transition = {}, l.transition = n._getPrefixedCSS("transition", "all " + n.animation.duration + "ms " + n.animation.easing + ", opacity " + n.animation.duration + "ms linear"), 
            n.animation.stagger = !!d("stagger"), n.animation.staggerDuration = parseInt(d("stagger") && d("stagger", !0).val ? d("stagger", !0).val : 100), 
            n._execFilter("_parseEffects", l);
        },
        _buildState: function(e) {
            var t, i = this;
            return i._execAction("_buildState", 0), t = {
                activeFilter: "" === i._activeFilter ? "none" : i._activeFilter,
                activeSort: e && i._newSortString ? i._newSortString : i._activeSort,
                fail: !i._$show.length && "" !== i._activeFilter,
                $targets: i._$targets,
                $show: i._$show,
                $hide: i._$hide,
                totalTargets: i._$targets.length,
                totalShow: i._$show.length,
                totalHide: i._$hide.length,
                display: e && i._newDisplay ? i._newDisplay : i.layout.display
            }, e ? i._execFilter("_buildState", t) : (i._state = t, void i._execAction("_buildState", 1));
        },
        _goMix: function(e) {
            var i = this, t = function() {
                i._chrome && 31 === i._chrome && n(i._$parent[0]), i._setInter(), o();
            }, o = function() {
                var e = window.pageYOffset, t = window.pageXOffset;
                document.documentElement.scrollHeight, i._getInterMixData(), i._setFinal(), i._getFinalMixData(), 
                window.pageYOffset !== e && window.scrollTo(t, e), i._prepTargets(), window.requestAnimationFrame ? requestAnimationFrame(s) : setTimeout(function() {
                    s();
                }, 20);
            }, s = function() {
                i._animateTargets(), 0 === i._targetsBound && i._cleanUp();
            }, n = function(e) {
                var t = e.parentElement, i = document.createElement("div"), o = document.createDocumentFragment();
                t.insertBefore(i, e), o.appendChild(e), t.replaceChild(e, i);
            }, r = i._buildState(!0);
            i._execAction("_goMix", 0, arguments), !i.animation.duration && (e = !1), i._mixing = !0, 
            i._$container.removeClass(i.layout.containerClassFail), "function" == typeof i.callbacks.onMixStart && i.callbacks.onMixStart.call(i._domNode, i._state, r, i), 
            i._$container.trigger("mixStart", [ i._state, r, i ]), i._getOrigMixData(), e && !i._suckMode ? window.requestAnimationFrame ? requestAnimationFrame(t) : t() : i._cleanUp(), 
            i._execAction("_goMix", 1, arguments);
        },
        _getTargetData: function(e, t) {
            var i;
            e.dataset[t + "PosX"] = e.offsetLeft, e.dataset[t + "PosY"] = e.offsetTop, this.animation.animateResizeTargets && (i = this._suckMode ? {
                marginBottom: "",
                marginRight: ""
            } : window.getComputedStyle(e), e.dataset[t + "MarginBottom"] = parseInt(i.marginBottom), 
            e.dataset[t + "MarginRight"] = parseInt(i.marginRight), e.dataset[t + "Width"] = e.offsetWidth, 
            e.dataset[t + "Height"] = e.offsetHeight);
        },
        _getOrigMixData: function() {
            var e = this, t = e._suckMode ? {
                boxSizing: ""
            } : window.getComputedStyle(e._$parent[0]), i = t.boxSizing || t[e._vendor + "BoxSizing"];
            e._incPadding = "border-box" === i, e._execAction("_getOrigMixData", 0), !e._suckMode && (e.effects = e._parseEffects()), 
            e._$toHide = e._$hide.filter(":visible"), e._$toShow = e._$show.filter(":hidden"), 
            e._$pre = e._$targets.filter(":visible"), e._startHeight = e._incPadding ? e._$parent.outerHeight() : e._$parent.height();
            for (var o = 0; o < e._$pre.length; o++) {
                var s = e._$pre[o];
                e._getTargetData(s, "orig");
            }
            e._execAction("_getOrigMixData", 1);
        },
        _setInter: function() {
            var e = this;
            e._execAction("_setInter", 0), e._changingLayout && e.animation.animateChangeLayout ? (e._$toShow.css("display", e._newDisplay), 
            e._changingClass && e._$container.removeClass(e.layout.containerClass).addClass(e._newClass)) : e._$toShow.css("display", e.layout.display), 
            e._execAction("_setInter", 1);
        },
        _getInterMixData: function() {
            var e = this;
            e._execAction("_getInterMixData", 0);
            for (var t = 0; t < e._$toShow.length; t++) {
                var i = e._$toShow[t];
                e._getTargetData(i, "inter");
            }
            for (t = 0; t < e._$pre.length; t++) {
                i = e._$pre[t];
                e._getTargetData(i, "inter");
            }
            e._execAction("_getInterMixData", 1);
        },
        _setFinal: function() {
            var e = this;
            e._execAction("_setFinal", 0), e._sorting && e._printSort(), e._$toHide.removeStyle("display"), 
            e._changingLayout && e.animation.animateChangeLayout && e._$pre.css("display", e._newDisplay), 
            e._execAction("_setFinal", 1);
        },
        _getFinalMixData: function() {
            var e = this;
            e._execAction("_getFinalMixData", 0);
            for (var t = 0; t < e._$toShow.length; t++) {
                var i = e._$toShow[t];
                e._getTargetData(i, "final");
            }
            for (t = 0; t < e._$pre.length; t++) {
                i = e._$pre[t];
                e._getTargetData(i, "final");
            }
            e._newHeight = e._incPadding ? e._$parent.outerHeight() : e._$parent.height(), e._sorting && e._printSort(!0), 
            e._$toShow.removeStyle("display"), e._$pre.css("display", e.layout.display), e._changingClass && e.animation.animateChangeLayout && e._$container.removeClass(e._newClass).addClass(e.layout.containerClass), 
            e._execAction("_getFinalMixData", 1);
        },
        _prepTargets: function() {
            var e = this, t = {
                _in: e._getPrefixedCSS("transform", e.effects.transformIn),
                _out: e._getPrefixedCSS("transform", e.effects.transformOut)
            };
            e._execAction("_prepTargets", 0), e.animation.animateResizeContainer && e._$parent.css("height", e._startHeight + "px");
            for (var i = 0; i < e._$toShow.length; i++) {
                var o = e._$toShow[i], s = c(o);
                o.style.opacity = e.effects.opacity, o.style.display = e._changingLayout && e.animation.animateChangeLayout ? e._newDisplay : e.layout.display, 
                s.css(t._in), e.animation.animateResizeTargets && (o.style.width = o.dataset.finalWidth + "px", 
                o.style.height = o.dataset.finalHeight + "px", o.style.marginRight = -(o.dataset.finalWidth - o.dataset.interWidth) + 1 * o.dataset.finalMarginRight + "px", 
                o.style.marginBottom = -(o.dataset.finalHeight - o.dataset.interHeight) + 1 * o.dataset.finalMarginBottom + "px");
            }
            for (i = 0; i < e._$pre.length; i++) {
                o = e._$pre[i], s = c(o);
                var n = o.dataset.origPosX - o.dataset.interPosX, r = o.dataset.origPosY - o.dataset.interPosY;
                t = e._getPrefixedCSS("transform", "translate(" + n + "px," + r + "px)");
                s.css(t), e.animation.animateResizeTargets && (o.style.width = o.dataset.origWidth + "px", 
                o.style.height = o.dataset.origHeight + "px", o.dataset.origWidth - o.dataset.finalWidth && (o.style.marginRight = -(o.dataset.origWidth - o.dataset.interWidth) + 1 * o.dataset.origMarginRight + "px"), 
                o.dataset.origHeight - o.dataset.finalHeight && (o.style.marginBottom = -(o.dataset.origHeight - o.dataset.interHeight) + 1 * o.dataset.origMarginBottom + "px"));
            }
            e._execAction("_prepTargets", 1);
        },
        _animateTargets: function() {
            var e = this;
            e._execAction("_animateTargets", 0), e._targetsDone = 0, e._targetsBound = 0, e._$parent.css(e._getPrefixedCSS("perspective", e.animation.perspectiveDistance + "px")).css(e._getPrefixedCSS("perspective-origin", e.animation.perspectiveOrigin)), 
            e.animation.animateResizeContainer && e._$parent.css(e._getPrefixedCSS("transition", "height " + e.animation.duration + "ms ease")).css("height", e._newHeight + "px");
            for (var t = 0; t < e._$toShow.length; t++) {
                var i = e._$toShow[t], o = c(i), s = {
                    x: i.dataset.finalPosX - i.dataset.interPosX,
                    y: i.dataset.finalPosY - i.dataset.interPosY
                }, n = e._getDelay(t), r = {};
                i.style.opacity = "";
                for (var a = 0; a < 2; a++) {
                    var l = 0 === a ? l = e._prefix : "";
                    e._ff && e._ff <= 20 && (r[l + "transition-property"] = "all", r[l + "transition-timing-function"] = e.animation.easing + "ms", 
                    r[l + "transition-duration"] = e.animation.duration + "ms"), r[l + "transition-delay"] = n + "ms", 
                    r[l + "transform"] = "translate(" + s.x + "px," + s.y + "px)";
                }
                (e.effects.transform || e.effects.opacity) && e._bindTargetDone(o), e._ff && e._ff <= 20 ? o.css(r) : o.css(e.effects.transition).css(r);
            }
            for (t = 0; t < e._$pre.length; t++) {
                i = e._$pre[t], o = c(i), s = {
                    x: i.dataset.finalPosX - i.dataset.interPosX,
                    y: i.dataset.finalPosY - i.dataset.interPosY
                }, n = e._getDelay(t);
                (i.dataset.finalPosX !== i.dataset.origPosX || i.dataset.finalPosY !== i.dataset.origPosY) && e._bindTargetDone(o), 
                o.css(e._getPrefixedCSS("transition", "all " + e.animation.duration + "ms " + e.animation.easing + " " + n + "ms")), 
                o.css(e._getPrefixedCSS("transform", "translate(" + s.x + "px," + s.y + "px)")), 
                e.animation.animateResizeTargets && (i.dataset.origWidth - i.dataset.finalWidth && 1 * i.dataset.finalWidth && (i.style.width = i.dataset.finalWidth + "px", 
                i.style.marginRight = -(i.dataset.finalWidth - i.dataset.interWidth) + 1 * i.dataset.finalMarginRight + "px"), 
                i.dataset.origHeight - i.dataset.finalHeight && 1 * i.dataset.finalHeight && (i.style.height = i.dataset.finalHeight + "px", 
                i.style.marginBottom = -(i.dataset.finalHeight - i.dataset.interHeight) + 1 * i.dataset.finalMarginBottom + "px"));
            }
            e._changingClass && e._$container.removeClass(e.layout.containerClass).addClass(e._newClass);
            for (t = 0; t < e._$toHide.length; t++) {
                i = e._$toHide[t], o = c(i), n = e._getDelay(t);
                var d = {};
                for (a = 0; a < 2; a++) {
                    d[(l = 0 === a ? l = e._prefix : "") + "transition-delay"] = n + "ms", d[l + "transform"] = e.effects.transformOut, 
                    d.opacity = e.effects.opacity;
                }
                o.css(e.effects.transition).css(d), (e.effects.transform || e.effects.opacity) && e._bindTargetDone(o);
            }
            e._execAction("_animateTargets", 1);
        },
        _bindTargetDone: function(t) {
            var i = this, o = t[0];
            i._execAction("_bindTargetDone", 0, arguments), o.dataset.bound || (o.dataset.bound = !0, 
            i._targetsBound++, t.on("webkitTransitionEnd.mixItUp transitionend.mixItUp", function(e) {
                (-1 < e.originalEvent.propertyName.indexOf("transform") || -1 < e.originalEvent.propertyName.indexOf("opacity")) && c(e.originalEvent.target).is(i.selectors.target) && (t.off(".mixItUp"), 
                o.dataset.bound = "", i._targetDone());
            })), i._execAction("_bindTargetDone", 1, arguments);
        },
        _targetDone: function() {
            var e = this;
            e._execAction("_targetDone", 0), e._targetsDone++, e._targetsDone === e._targetsBound && e._cleanUp(), 
            e._execAction("_targetDone", 1);
        },
        _cleanUp: function() {
            var e = this, t = e.animation.animateResizeTargets ? "transform opacity width height margin-bottom margin-right" : "transform opacity";
            e._execAction("_cleanUp", 0), e._changingLayout ? e._$show.css("display", e._newDisplay) : e._$show.css("display", e.layout.display), 
            e._$targets.css(e._brake), e._$targets.removeStyle(t, e._prefix).removeAttr("data-inter-pos-x data-inter-pos-y data-final-pos-x data-final-pos-y data-orig-pos-x data-orig-pos-y data-orig-height data-orig-width data-final-height data-final-width data-inter-width data-inter-height data-orig-margin-right data-orig-margin-bottom data-inter-margin-right data-inter-margin-bottom data-final-margin-right data-final-margin-bottom"), 
            e._$hide.removeStyle("display"), e._$parent.removeStyle("height transition perspective-distance perspective perspective-origin-x perspective-origin-y perspective-origin perspectiveOrigin", e._prefix), 
            e._sorting && (e._printSort(), e._activeSort = e._newSortString, e._sorting = !1), 
            e._changingLayout && (e._changingDisplay && (e.layout.display = e._newDisplay, e._changingDisplay = !1), 
            e._changingClass && (e._$parent.removeClass(e.layout.containerClass).addClass(e._newClass), 
            e.layout.containerClass = e._newClass, e._changingClass = !1), e._changingLayout = !1), 
            e._refresh(), e._buildState(), e._state.fail && e._$container.addClass(e.layout.containerClassFail), 
            e._$show = c(), e._$hide = c(), window.requestAnimationFrame && requestAnimationFrame(function() {
                e._$targets.removeStyle("transition", e._prefix);
            }), e._mixing = !1, "function" == typeof e.callbacks._user && e.callbacks._user.call(e._domNode, e._state, e), 
            "function" == typeof e.callbacks.onMixEnd && e.callbacks.onMixEnd.call(e._domNode, e._state, e), 
            e._$container.trigger("mixEnd", [ e._state, e ]), e._state.fail && ("function" == typeof e.callbacks.onMixFail && e.callbacks.onMixFail.call(e._domNode, e._state, e), 
            e._$container.trigger("mixFail", [ e._state, e ])), e._loading && ("function" == typeof e.callbacks.onMixLoad && e.callbacks.onMixLoad.call(e._domNode, e._state, e), 
            e._$container.trigger("mixLoad", [ e._state, e ])), e._queue.length && (e._execAction("_queue", 0), 
            e.multiMix(e._queue[0][0], e._queue[0][1], e._queue[0][2]), e._queue.splice(0, 1)), 
            e._execAction("_cleanUp", 1), e._loading = !1;
        },
        _getPrefixedCSS: function(e, t, i) {
            var o = {}, s = "", n = -1;
            for (n = 0; n < 2; n++) o[(s = 0 === n ? this._prefix : "") + e] = i ? s + t : t;
            return this._execFilter("_getPrefixedCSS", o, arguments);
        },
        _getDelay: function(e) {
            var t = this, i = "function" == typeof t.animation.staggerSequence ? t.animation.staggerSequence.call(t._domNode, e, t._state) : e, o = t.animation.stagger ? i * t.animation.staggerDuration : 0;
            return t._execFilter("_getDelay", o, arguments);
        },
        _parseMultiMixArgs: function(e) {
            for (var t = {
                command: null,
                animate: this.animation.enable,
                callback: null
            }, i = 0; i < e.length; i++) {
                var o = e[i];
                null !== o && ("object" == typeof o || "string" == typeof o ? t.command = o : "boolean" == typeof o ? t.animate = o : "function" == typeof o && (t.callback = o));
            }
            return this._execFilter("_parseMultiMixArgs", t, arguments);
        },
        _parseInsertArgs: function(e) {
            for (var t = {
                index: 0,
                $object: c(),
                multiMix: {
                    filter: this._state.activeFilter
                },
                callback: null
            }, i = 0; i < e.length; i++) {
                var o = e[i];
                "number" == typeof o ? t.index = o : "object" == typeof o && o instanceof c ? t.$object = o : "object" == typeof o && this._helpers._isElement(o) ? t.$object = c(o) : "object" == typeof o && null !== o ? t.multiMix = o : "boolean" != typeof o || o ? "function" == typeof o && (t.callback = o) : t.multiMix = !1;
            }
            return this._execFilter("_parseInsertArgs", t, arguments);
        },
        _execAction: function(e, t, i) {
            var o = this, s = t ? "post" : "pre";
            if (!o._actions.isEmptyObject && o._actions.hasOwnProperty(e)) for (var n in o._actions[e][s]) o._actions[e][s][n].call(o, i);
        },
        _execFilter: function(e, t, i) {
            var o = this;
            if (o._filters.isEmptyObject || !o._filters.hasOwnProperty(e)) return t;
            for (var s in o._filters[e]) return o._filters[e][s].call(o, i);
        },
        _helpers: {
            _camelCase: function(e) {
                return e.replace(/-([a-z])/g, function(e) {
                    return e[1].toUpperCase();
                });
            },
            _isElement: function(e) {
                return window.HTMLElement ? e instanceof HTMLElement : null !== e && 1 === e.nodeType && "string" === e.nodeName;
            }
        },
        isMixing: function() {
            return this._execFilter("isMixing", this._mixing);
        },
        filter: function() {
            var e = this._parseMultiMixArgs(arguments);
            this._clicking && (this._toggleString = ""), this.multiMix({
                filter: e.command
            }, e.animate, e.callback);
        },
        sort: function() {
            var e = this._parseMultiMixArgs(arguments);
            this.multiMix({
                sort: e.command
            }, e.animate, e.callback);
        },
        changeLayout: function() {
            var e = this._parseMultiMixArgs(arguments);
            this.multiMix({
                changeLayout: e.command
            }, e.animate, e.callback);
        },
        multiMix: function() {
            var e = this, t = e._parseMultiMixArgs(arguments);
            if (e._execAction("multiMix", 0, arguments), e._mixing) e.animation.queue && e._queue.length < e.animation.queueLimit ? (e._queue.push(arguments), 
            e.controls.enable && !e._clicking && e._updateControls(t.command), e._execAction("multiMixQueue", 1, arguments)) : ("function" == typeof e.callbacks.onMixBusy && e.callbacks.onMixBusy.call(e._domNode, e._state, e), 
            e._$container.trigger("mixBusy", [ e._state, e ]), e._execAction("multiMixBusy", 1, arguments)); else {
                e.controls.enable && !e._clicking && (e.controls.toggleFilterButtons && e._buildToggleArray(), 
                e._updateControls(t.command, e.controls.toggleFilterButtons)), e._queue.length < 2 && (e._clicking = !1), 
                delete e.callbacks._user, t.callback && (e.callbacks._user = t.callback);
                var i = t.command.sort, o = t.command.filter, s = t.command.changeLayout;
                e._refresh(), i && (e._newSort = e._parseSort(i), e._newSortString = i, e._sorting = !0, 
                e._sort()), o !== p && (o = "all" === o ? e.selectors.target : o, e._activeFilter = o), 
                e._filter(), s && (e._newDisplay = "string" == typeof s ? s : s.display || e.layout.display, 
                e._newClass = s.containerClass || "", (e._newDisplay !== e.layout.display || e._newClass !== e.layout.containerClass) && (e._changingLayout = !0, 
                e._changingClass = e._newClass !== e.layout.containerClass, e._changingDisplay = e._newDisplay !== e.layout.display)), 
                e._$targets.css(e._brake), e._goMix(t.animate ^ e.animation.enable ? t.animate : e.animation.enable), 
                e._execAction("multiMix", 1, arguments);
            }
        },
        insert: function() {
            var e = this, t = e._parseInsertArgs(arguments), i = "function" == typeof t.callback ? t.callback : null, o = document.createDocumentFragment(), s = (e._refresh(), 
            e._$targets.length ? t.index < e._$targets.length || !e._$targets.length ? e._$targets[t.index] : e._$targets[e._$targets.length - 1].nextElementSibling : e._$parent[0].children[0]);
            if (e._execAction("insert", 0, arguments), t.$object) {
                for (var n = 0; n < t.$object.length; n++) {
                    var r = t.$object[n];
                    o.appendChild(r), o.appendChild(document.createTextNode(" "));
                }
                e._$parent[0].insertBefore(o, s);
            }
            e._execAction("insert", 1, arguments), "object" == typeof t.multiMix && e.multiMix(t.multiMix, i);
        },
        prepend: function() {
            var e = this._parseInsertArgs(arguments);
            this.insert(0, e.$object, e.multiMix, e.callback);
        },
        append: function() {
            var e = this._parseInsertArgs(arguments);
            this.insert(this._state.totalTargets, e.$object, e.multiMix, e.callback);
        },
        getOption: function(e) {
            return e ? this._execFilter("getOption", function(e, t) {
                for (var i = t.split("."), o = i.pop(), s = i.length, n = 1, r = i[0] || t; (e = e[r]) && n < s; ) r = i[n], 
                n++;
                return e !== p ? e[o] !== p ? e[o] : e : void 0;
            }(this, e), arguments) : this;
        },
        setOptions: function(e) {
            this._execAction("setOptions", 0, arguments), "object" == typeof e && c.extend(!0, this, e), 
            this._execAction("setOptions", 1, arguments);
        },
        getState: function() {
            return this._execFilter("getState", this._state, this);
        },
        forceRefresh: function() {
            this._refresh(!1, !0);
        },
        destroy: function(e) {
            var t = this, i = c.MixItUp.prototype._bound._filter, o = c.MixItUp.prototype._bound._sort;
            t._execAction("destroy", 0, arguments), t._$body.add(c(t.selectors.sort)).add(c(t.selectors.filter)).off(".mixItUp");
            for (var s = 0; s < t._$targets.length; s++) {
                var n = t._$targets[s];
                e && (n.style.display = ""), delete n.mixParent;
            }
            t._execAction("destroy", 1, arguments), i[t.selectors.filter] && 1 < i[t.selectors.filter] ? i[t.selectors.filter]-- : 1 === i[t.selectors.filter] && delete i[t.selectors.filter], 
            o[t.selectors.sort] && 1 < o[t.selectors.sort] ? o[t.selectors.sort]-- : 1 === o[t.selectors.sort] && delete o[t.selectors.sort], 
            delete c.MixItUp.prototype._instances[t._id];
        }
    }, c.fn.mixItUp = function() {
        var e, i = arguments, o = [];
        return e = this.each(function() {
            if (i && "string" == typeof i[0]) {
                var e = c.MixItUp.prototype._instances[this.id];
                if ("isLoaded" === i[0]) o.push(!!e); else {
                    var t = e[i[0]](i[1], i[2], i[3]);
                    t !== p && o.push(t);
                }
            } else !function(e, t) {
                var i = new c.MixItUp();
                i._execAction("_instantiate", 0, arguments), e.id = e.id ? e.id : "MixItUp" + ("00000" + (16777216 * Math.random() << 0).toString(16)).substr(-6).toUpperCase(), 
                i._instances[e.id] || (i._instances[e.id] = i)._init(e, t), i._execAction("_instantiate", 1, arguments);
            }(this, i[0]);
        }), o.length ? 1 < o.length ? o : o[0] : e;
    }, c.fn.removeStyle = function(n, r) {
        return r = r || "", this.each(function() {
            for (var e = this, t = n.split(" "), i = 0; i < t.length; i++) for (var o = 0; o < 4; o++) {
                switch (o) {
                  case 0:
                    var s = t[i];
                    break;

                  case 1:
                    s = c.MixItUp.prototype._helpers._camelCase(s);
                    break;

                  case 2:
                    s = r + t[i];
                    break;

                  case 3:
                    s = c.MixItUp.prototype._helpers._camelCase(r + t[i]);
                }
                if (e.style[s] !== p && "unknown" != typeof e.style[s] && 0 < e.style[s].length && (e.style[s] = ""), 
                !r && 1 === o) break;
            }
            e.attributes && e.attributes.style && e.attributes.style !== p && "" === e.attributes.style.value && e.attributes.removeNamedItem("style");
        });
    };
}(jQuery), function(e) {
    var t = !1;
    if ("function" == typeof define && define.amd && (define(e), t = !0), "object" == typeof exports && (module.exports = e(), 
    t = !0), !t) {
        var i = window.Cookies, o = window.Cookies = e();
        o.noConflict = function() {
            return window.Cookies = i, o;
        };
    }
}(function() {
    function g() {
        for (var e = 0, t = {}; e < arguments.length; e++) {
            var i = arguments[e];
            for (var o in i) t[o] = i[o];
        }
        return t;
    }
    return function e(f) {
        function h(e, t, i) {
            var o;
            if ("undefined" != typeof document) {
                if (1 < arguments.length) {
                    if ("number" == typeof (i = g({
                        path: "/"
                    }, h.defaults, i)).expires) {
                        var s = new Date();
                        s.setMilliseconds(s.getMilliseconds() + 864e5 * i.expires), i.expires = s;
                    }
                    i.expires = i.expires ? i.expires.toUTCString() : "";
                    try {
                        o = JSON.stringify(t), /^[\{\[]/.test(o) && (t = o);
                    } catch (e) {}
                    t = f.write ? f.write(t, e) : encodeURIComponent(String(t)).replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent), 
                    e = (e = (e = encodeURIComponent(String(e))).replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent)).replace(/[\(\)]/g, escape);
                    var n = "";
                    for (var r in i) i[r] && (n += "; " + r, !0 !== i[r] && (n += "=" + i[r]));
                    return document.cookie = e + "=" + t + n;
                }
                e || (o = {});
                for (var a = document.cookie ? document.cookie.split("; ") : [], l = /(%[0-9A-Z]{2})+/g, d = 0; d < a.length; d++) {
                    var c = a[d].split("="), p = c.slice(1).join("=");
                    this.json || '"' !== p.charAt(0) || (p = p.slice(1, -1));
                    try {
                        var u = c[0].replace(l, decodeURIComponent);
                        if (p = f.read ? f.read(p, u) : f(p, u) || p.replace(l, decodeURIComponent), this.json) try {
                            p = JSON.parse(p);
                        } catch (e) {}
                        if (e === u) {
                            o = p;
                            break;
                        }
                        e || (o[u] = p);
                    } catch (e) {}
                }
                return o;
            }
        }
        return (h.set = h).get = function(e) {
            return h.call(h, e);
        }, h.getJSON = function() {
            return h.apply({
                json: !0
            }, [].slice.call(arguments));
        }, h.defaults = {}, h.remove = function(e, t) {
            h(e, "", g(t, {
                expires: -1
            }));
        }, h.withConverter = e, h;
    }(function() {});
}), $(document).ready(function() {
    $(".slide-up--static").each(function(e) {
        $(this).addClass("slide-up--loaded");
    });
}), jQuery(document).ready(function(s) {
     s.fn.isOnScreen = function() {
        var e = s(window), t = {
            top: e.scrollTop()
        };
        t.bottom = t.top + e.height() - 100;
        var i = this.offset();
        return i.bottom = i.top + this.outerHeight(), !(t.bottom < i.top || t.top > i.bottom);
    }, s(window).scroll(function() {
        s(".off-screen").each(function(e) {
            s(this).isOnScreen() && s(this).removeClass("off-screen--hide");
        }), s(".slide-up--scroll").each(function(e) {
            s(this).isOnScreen() && s(this).addClass("slide-up--loaded");
        });
    }), s(".off-screen--static, .page-id-2658 .news-grid .off-screen").each(function(e) {
        s(this).removeClass("off-screen--hide off-screen--static");
    });
});