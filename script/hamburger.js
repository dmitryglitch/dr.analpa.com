jQuery(document).ready(function(s) {
  s(".menu-wrap__open").on("click", function() {
    s(".menu-main-menu-container").toggleClass("menu-open"),
      s(".hamburger__inner").toggleClass("open-menu");
  });

  s("#anchor").on("click", function () {
      s(".menu-main-menu-container").toggleClass("menu-open"),
        s(".hamburger__inner").toggleClass("open-menu");
  })
});
