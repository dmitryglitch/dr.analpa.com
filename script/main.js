jQuery(document).ready(function($) {
  function countup(className) {
    //className - имя класса, в котором есть число
    var countBlockTop = $("." + className).offset().top; //смещение блока с числом относительно верхнего края
    var windowHeight = window.innerHeight; //высота окна браузера
    var show = true; // отвечает, что если один раз счетчик сработает, больше не срабатывал

    $(window).scroll(function() {
      if (show && countBlockTop < $(window).scrollTop() + windowHeight) {
        show = false; //если мы видим число, то больше его не надо показывать

        $("." + className).spincrement({
          //вызов плагина с параметрами
          from: 1, //начинать с 1
          duration: 1500 //задержка счетчика
        });
      }
    });
  }

  $(document).ready(function() {
    $("#anchor").on("click", function(event) {
      event.preventDefault();
      var id = $(this).attr("href"),
        top = $(id).offset().top;
      $("body,html").animate({ scrollTop: top }, 1500);
    });
  });

  countup("count");
});

function horizontal_scroll_left() {
  var horizontal = $(".horizontal-scroll--left");

  if (horizontal) {
    var speed = 20;
    var direction = "left";
    var startPosition = horizontal.position();
    $(window).scroll(function() {
      var st = $(this).scrollTop();
      var newPosition = st * (speed / 100);
      horizontal.css({
        left: newPosition-100
      });
    });
  }
}

function horizontal_scroll_right() {
  var horizontal = $(".horizontal-scroll--right");
  if (horizontal) {
    var speed = 15;
    var direction = "right";
    var startPosition = horizontal.position();
    $(window).scroll(function() {
      var st = $(this).scrollTop()-560;
      var newPosition = st * (speed / 100);
      horizontal.css({
        right: newPosition
      });
    });
  }
}
horizontal_scroll_left();
horizontal_scroll_right();
